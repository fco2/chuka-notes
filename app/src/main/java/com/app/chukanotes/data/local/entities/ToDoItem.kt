package com.app.chukanotes.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.time.OffsetDateTime

@Entity(
    tableName = "to_do_item",
    foreignKeys = [
        ForeignKey(
            entity = NoteEntity::class,
            parentColumns = ["id"],
            childColumns = ["note_id"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class ToDoItem(
    @ColumnInfo(name = "note_id", index = true)
    var noteId: Long = 0L,
    var message: String = "",
    @ColumnInfo(name = "is_completed")
    var isCompleted: Boolean = false,
    @ColumnInfo(name = "date_completed")
    var dateCompleted: OffsetDateTime? = null,
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
){
    override fun toString(): String {
        return "id: $id, noteId: $noteId, message: $message, isCompleted: $isCompleted, dateCompleted: $dateCompleted"
    }
}