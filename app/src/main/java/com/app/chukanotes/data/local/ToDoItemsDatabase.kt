package com.app.chukanotes.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.chukanotes.data.local.entities.LabelEntity
import com.app.chukanotes.data.local.entities.NoteEntity
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.data.local.util.DateConverter

@Database(
    entities = [NoteEntity::class, ToDoItem::class, LabelEntity::class],
    version = 9,
        exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class ToDoItemsDatabase: RoomDatabase() {
    abstract val toDoItemsDao: NoteAndToDoItemDao

    companion object{
        const val DATABASE_NAME = "to_do_items_db"
    }
}