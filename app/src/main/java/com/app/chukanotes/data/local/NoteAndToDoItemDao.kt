package com.app.chukanotes.data.local

import androidx.room.*
import com.app.chukanotes.data.local.entities.LabelEntity
import com.app.chukanotes.data.local.entities.NoteEntity
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.RankWithPinnedState

/*
****** note this below  *******
SQLite. Unlike the above database types, SQLite considers NULLs to be smaller than any other value.
If you sort a column with NULL values in ascending order, the NULLs will come first.

So,  we would give the top items the higher rank and order in desc order.
With a 10 item list, if you have no ranking, it will order by date, but if you have ranking, your ranking will be set by this for example -->

1, 2, 3, 4, [5], 6, 7, 8, 9, 10
I move 5 to before 2.
1, [5], 2, 3, 4, ....
I will rank everything, give 1 a rank of 10, and 5 a rank of 9, then 2, a rank of 8.. and so on..
 */

@Dao
interface NoteAndToDoItemDao {

    //get all notes -
    @Transaction
    @Query("SELECT * FROM note ORDER BY rank desc, datetime(last_updated) desc")
    suspend fun getAllNotes(): List<NoteWithToDoItemAndLabelTitle>

    //get a note
    @Transaction
    @Query("SELECT * FROM note WHERE id = :noteId LIMIT 1")
    suspend fun getNote(noteId: Long): NoteWithToDoItemAndLabelTitle?

    @Query("SELECT * FROM note WHERE id = :noteId LIMIT 1")
    suspend fun getSingleNote(noteId: Long): NoteEntity?

    //get a todo_item
    @Query("SELECT * FROM to_do_item WHERE note_id = :noteId LIMIT 1")
    suspend fun getToDoItemForPlainNote(noteId: Long): ToDoItem?

    //insert note
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertNote(note: NoteEntity): Long

    //update all notes for re-ordered items
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertAllNotesWhereNotesIn(notes: List<NoteEntity>)

    @Query("SELECT * FROM to_do_item WHERE note_id = :noteId")
    suspend fun getAllItemsFromToDoWith(noteId: Long): List<ToDoItem>

    //upsert to-do item
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertToDoItem(toDoItem: ToDoItem): Long

    //upsert multiple toDoItems
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertToDoItemsFor(toDoItems: List<ToDoItem>)

    @Transaction
    suspend fun upsertNoteWithTodos(note: NoteEntity, toDoItems: List<ToDoItem>){
        upsertNote(note)
        upsertToDoItemsFor(toDoItems)
    }

    @Transaction
    suspend fun upsertNoteWithSingleTodo(note: NoteEntity, toDoItem: ToDoItem): Long {
        val noteId = upsertNote(note)
        toDoItem.noteId = noteId
        upsertToDoItem(toDoItem)
        return noteId
    }

    //delete note
    @Delete
    suspend fun deleteNote(note: NoteEntity)

    @Delete
    suspend fun deleteNotesWithNoteObject(notes: List<NoteEntity>)

    //delete all notes
    @Query("DELETE FROM note")
    suspend fun deleteAllNotes()

    //delete selected notes
    @Query("DELETE FROM note WHERE id IN (:noteIds)")
    suspend fun deleteNotesWhere(noteIds: List<Long?>)

    //delete selected todo_items who's note ids are..
    @Query("DELETE FROM to_do_item WHERE note_id IN (:noteIds)")
    suspend fun deleteToDoItemsWhere(noteIds: List<Long?>)

    //For clean up
    @Query("DELETE FROM to_do_item")
    suspend fun deleteAllToDoItems()

    //delete all to-do items in a note
    @Query("DELETE FROM to_do_item WHERE note_id = :noteId")
    suspend fun deleteAllToDoItemsFromNote(noteId: Long)

    //delete a to-do item from a note
    @Query("DELETE FROM to_do_item WHERE note_id = :noteId AND id = :toDoId")
    suspend fun deleteToDoItemFromNote(noteId: Long, toDoId: Long)

    //get all Labels
    @Query("SELECT * FROM label")
    suspend fun getAllLabels(): List<LabelEntity>

    //Add a label
    @Insert(onConflict= OnConflictStrategy.REPLACE)
    suspend fun addLabel(label: LabelEntity): Long

   //get all ranks with pinned state
    @Query("SELECT rank, is_pinned AS isPinned FROM note ORDER BY rank, isPinned DESC")
    suspend fun getAllRankWithPinnedState(): List<RankWithPinnedState>

    @Query("SELECT MAX(rank) FROM note WHERE is_pinned = 0")
    suspend fun getHighestRankForUnPinnedState(): Long?

    @Query("SELECT title FROM label WHERE note_id = :id")
    suspend fun getLabelTitleWhereNoteIdIs(id: Long): String?

    //Delete a label
    @Query("DELETE FROM label WHERE title = :labelTitle")
    suspend fun deleteLabel(labelTitle: String)

    @Query("DELETE FROM label")
    suspend fun deleteAllLabels()
}
