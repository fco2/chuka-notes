package com.app.chukanotes.data.local

import androidx.room.Embedded
import androidx.room.Relation
import com.app.chukanotes.data.local.entities.LabelEntity
import com.app.chukanotes.data.local.entities.NoteEntity
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.NoteWithPreview

data class NoteWithToDoItemAndLabelTitle (
    @Embedded val note: NoteEntity,
    @Relation(
            parentColumn = "id",
            entityColumn = "note_id"
    )
    val toDoItems: List<ToDoItem>,

    @Relation(
        parentColumn = "id",
        entityColumn = "note_id"
    )
    val label: LabelEntity? = null
){
    fun toNoteWithPreview(): NoteWithPreview {
        val filteredToDoItems = this.toDoItems.filter { property -> !property.isCompleted }
        val previewOneLocal: String = if(filteredToDoItems.isNotEmpty()) filteredToDoItems[0].message else ""
        val previewTwoLocal: String = if(filteredToDoItems.size > 1) filteredToDoItems[1].message else ""
        val previewThreeLocal: String = if(filteredToDoItems.size > 2) filteredToDoItems[2].message else ""
        return NoteWithPreview(
            note = this.note.toNote(),
            labelName = if(this.label == null) "" else this.label.title!!,
            previewOne = previewOneLocal,
            previewTwo = previewTwoLocal,
            previewThree = previewThreeLocal
        )
    }
}