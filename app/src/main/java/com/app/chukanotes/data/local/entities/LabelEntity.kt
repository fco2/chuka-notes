package com.app.chukanotes.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.app.chukanotes.domain.models.Label

// TODO: to fix deleted labels if two are added for a single note, add date column,
//  sort getLabels by date created DESC, get first. Then insert
@Entity(tableName = "label",
        indices = [Index(value = ["note_id"], unique = true)]
)
data class LabelEntity(
        @ColumnInfo(name="title") // , index = true
        var title: String? = "",
        @ColumnInfo(name="note_id")
        var noteId: Long = -1L,
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null
) {
        fun toLabel(): Label {
                return Label(
                        id = id!!,
                        noteId = noteId,
                        title = title!!
                )
        }
}
