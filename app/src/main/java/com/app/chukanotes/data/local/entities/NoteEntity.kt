package com.app.chukanotes.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.app.chukanotes.domain.models.Note
import java.time.OffsetDateTime

@Entity(tableName = "note")
data class NoteEntity(
    var title: String = "<No Title>",
    var color: Long = 0xff263238, //Grey color for default
    @ColumnInfo(name = "last_updated")
    var lastUpdated: OffsetDateTime? = null,
    var rank: Long = 0L,
    @ColumnInfo(name = "note_format")
    var noteFormat: String? = "",
    @ColumnInfo(name = "is_pinned")
    var isPinned: Boolean = false,
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
) {
    fun toNote(): Note {
        return Note(
            title = title,
            color = color,
            lastUpdated = lastUpdated,
            rank = rank,
            noteFormat = noteFormat,
            isPinned = isPinned,
            id = id
        )
    }
}