package com.app.chukanotes.data.local.util

import androidx.room.TypeConverter
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter


class DateConverter {
    private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    @FromJson
    @TypeConverter
    fun toOffsetDateTime(date: String?): OffsetDateTime?{
        return date?.let {
            return formatter.parse(date, OffsetDateTime::from)
        }
    }

    @ToJson
    @TypeConverter
    fun fromOffsetDateTime(date: OffsetDateTime?): String?{
        return date?.format(formatter)
    }
}

// swap # with ___
class HashSymbolAdapter {
    @ToJson
    fun toJson(color: String): String {
        return color.replace("#", "___")
    }

    @FromJson fun fromJson(rgb: String): String {
        return rgb.replace("___", "#")
    }
}

// swap + with ##### for OffsetDateTime
//The ISO date-time formatter that formats or parses a date-time with an offset, such as '2011-12-03T10:15:30+01:00'.
class CustomDateTimeAdapter {
    private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME  // DateTimeFormatter.ISO_OFFSET_DATE_TIME

    @FromJson
    fun fromJson(lastUpdatedString: String): OffsetDateTime {
        return OffsetDateTime.parse(lastUpdatedString.replace("#####", "+"), formatter)
    }

    @ToJson
    fun toJson(lastUpdated: OffsetDateTime): String {
        return formatter.format(lastUpdated).replace("+", "#####")
    }
}

// swap + char with ~~~
class PlusCharacterAdapter {
    @FromJson
    fun fromJson(value: String): String {
        return value.replace("~~~", "+")
    }

    @ToJson
    fun toJson(value: String): String {
        return value.replace("+", "~~~")
    }
}





