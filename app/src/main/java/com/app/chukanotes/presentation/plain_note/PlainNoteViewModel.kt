package com.app.chukanotes.presentation.plain_note

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.chukanotes.core.Resource
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.Label
import com.app.chukanotes.domain.repositories.NoteAndToDoRepository
import com.app.chukanotes.presentation.util.UiEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import javax.inject.Inject

@HiltViewModel
class PlainNoteViewModel @Inject constructor(
    private val repository: NoteAndToDoRepository,
    private val savedStateHandle: SavedStateHandle
    ): ViewModel()
{
    var state by mutableStateOf(PlainNoteState())

    private val _uiEvent = Channel<UiEvent>()
    val uiEvent = _uiEvent.receiveAsFlow()

    private var _labelList = emptyList<String>().toMutableStateList()
    val labelList: MutableSet<String>
        get() = _labelList.toMutableSet()

    private var job: Job? = null

    init {
        loadData()
    }

    private fun loadData() = viewModelScope.launch{
        //set the note object from savedStateHandle
        val id = savedStateHandle.get<Long>("noteId")!!
        val noteFromSavedState = repository.getSingleNote(id)?.toNote()!!
        state = state.copy(note = noteFromSavedState, isLoading = true, isScreenContentInitialized = false)

        viewModelScope.launch{
            repository.getToDoItemForPlainNote(noteFromSavedState.id!!)?.apply {
                state = state.copy(
                    titleText = noteFromSavedState.title,
                    isTitleEmpty = noteFromSavedState.title.isEmpty(),
                    currentNoteColor = noteFromSavedState.color,
                    plainNoteContent = this.let { message },
                    isPinned = noteFromSavedState.isPinned,
                    plainNoteId = this.id!!
                )
            }
            state = state.copy(isLoading = false, isScreenContentInitialized = true)
        }
    }

    fun onEvent(event: PlainNoteEvent){
        when(event){
            is PlainNoteEvent.OnContentTextChanged -> {
                state = state.copy(plainNoteContent = event.newTextValue)
                job?.cancel()
                job = viewModelScope.launch {
                    delay(3000L)
                    repository.upsertNoteWithTodos(
                        state.note.also { it.title = state.titleText; it.lastUpdated = OffsetDateTime.now() },
                        listOf(ToDoItem(
                            id = state.plainNoteId,
                            noteId = state.note.id!!,
                            message = state.plainNoteContent))
                    )
                }
            }
            is PlainNoteEvent.OnTitleTextChanged -> {
                val updatedNote = state.note.also { it.lastUpdated = OffsetDateTime.now(); it.title = event.newTextValue}
                state = state.copy(note = updatedNote, isTitleEmpty = updatedNote.title.isBlank(),
                    titleText = event.newTextValue)
                job?.cancel()
                job = viewModelScope.launch {
                    delay(3000L)
                    repository.upsertNoteWithTodos(
                        state.note.also { it.title = state.titleText },
                        listOf(ToDoItem(
                            id = state.plainNoteId,
                            noteId = state.note.id!!,
                            message = state.plainNoteContent))
                    )
                }
            }
            PlainNoteEvent.OnBackPressed -> {
                job?.cancel()
                job = viewModelScope.launch {
                    //Timber.d("CHUKA-note: inside viewModelScope ")
                    state = state.copy(isLoading = true)
                    //only save note and to-do if both are not empty
                    if(isNoteAndTodosEmpty()){
                        //deleting note will cascade the todos
                        repository.deleteNote(state.note.toNoteEntity())
                    }else{
                        //didn't update lastUpdated as that will be handled when note contents actually change
                        repository.upsertNoteWithTodos(
                            state.note.also {
                                it.title = state.titleText; it.lastUpdated = it.lastUpdated},
                            listOf(ToDoItem(
                                id = state.plainNoteId,
                                noteId = state.note.id!!,
                                // trimEnd() fixes bug with text deleting first word for text with trailing spaces due to focusRequester anomalies
                                message = state.plainNoteContent.trimEnd()
                            ))
                        )
                    }
                    //state = state.copy(isLoading = false)
                    _uiEvent.send(UiEvent.OnBackPressed())
                }
            }
            is PlainNoteEvent.OnNoteColorChanged -> {
                job?.cancel()
                job = viewModelScope.launch {
                    state = state.copy(
                        note = event.note.also {
                            it.color = event.newColor
                            it.lastUpdated = OffsetDateTime.now()
                            it.title = state.titleText },
                        currentNoteColor = event.newColor
                    )
                    repository.upsertNoteWithTodos(
                        state.note,
                        toDoItems = listOf(
                            ToDoItem(
                                noteId = state.note.id!!,
                                message = state.plainNoteContent,
                                id = state.plainNoteId
                            )
                        )
                    )
                }
            }
            is PlainNoteEvent.OnDeleteNote -> {
                job?.cancel()
                job = viewModelScope.launch {
                    state = state.copy(isLoading = true)
                    repository.deleteNote(state.note.toNoteEntity())
                    state = state.copy(isLoading = false)
                    _uiEvent.send(UiEvent.OnNavigateToNotesScreen)
                }
            }
            is PlainNoteEvent.OnDeleteLabel -> viewModelScope.launch{
                state = state.copy(isLoading = true)
                repository.deleteLabel(event.labelTitle)
                val tempList = mutableSetOf<String>()
                tempList.addAll(labelList)
                tempList.remove(event.labelTitle)
                _labelList.clear()
                _labelList = tempList.toMutableStateList()
                if(state.currentNoteLabel == event.labelTitle)
                    state = state.copy(currentNoteLabel = "") //clean up references
                state = state.copy(isLoading = false)
            }
            is PlainNoteEvent.OnLabelCheckboxChecked -> viewModelScope.launch{
                state = if(event.isChecked){
                    val newLabel = Label(title = event.labelText, noteId = state.note.id!!)
                    repository.addLabel(newLabel)
                    state.copy(currentNoteLabel = event.labelText)
                }else{
                    repository.deleteLabel(state.currentNoteLabel) //first delete
                    val label = Label(title = state.currentNoteLabel, noteId = -1)
                    repository.addLabel(label)
                    state.copy(currentNoteLabel = "")
                }
            }
            is PlainNoteEvent.OnLabelModalOpened -> {
                var gotToCollectBlock = false //used this flag as label list can be empty when first launched
                repository.getAllLabels().onEach { result ->
                    when(result){
                        is Resource.Success -> {
                            gotToCollectBlock = true
                            _labelList.addAll(result.data!!.map { it.title }.distinct())
                            //set currentNoteLabel here
                            result.data.forEach { label ->
                                if(label.noteId == state.note.id){
                                    state = state.copy(currentNoteLabel = label.title)
                                    return@forEach
                                }
                            }
                            state = state.copy(isLoading = false)
                            _uiEvent.send(UiEvent.OnLabelModalOpened)
                        }
                        is Resource.Loading -> state = state.copy(isLoading = true)
                        is Resource.Error -> Unit
                    }
                }.launchIn(viewModelScope)
                //ensures the modal opens for no labels created state
                viewModelScope.launch { if(!gotToCollectBlock) _uiEvent.send(UiEvent.OnLabelModalOpened) }
            }
            is PlainNoteEvent.OnLabelTextFieldChanged -> { state = state.copy(currentEditingLabelText = event.labelText) }
            is PlainNoteEvent.OnSaveNewLabel -> viewModelScope.launch{
                val newLabel = Label(title = state.currentEditingLabelText, noteId = state.note.id!!)
                repository.addLabel(newLabel)
                _labelList += state.currentEditingLabelText
                //set current note label before erasing it
                state = state.copy(currentNoteLabel = state.currentEditingLabelText)
                state = state.copy(currentEditingLabelText = "")
            }
            PlainNoteEvent.OnPinnedStateToggle -> {
                job?.cancel()
                job = viewModelScope.launch {
                    //save existing isPinned state
                    val isCurrentlyPinned = state.isPinned
                    //check pinned state and update db accordingly
                    repository.getAllRankWithPinnedState().onEach { result ->
                        when(result){
                            is Resource.Success -> {
                                val sortedData = result.data!!.toMutableList()
                                sortedData.sortWith(compareByDescending { it.rank })
                                var newRank = -1L
                                //create updated Note object
                                val updatedNote = state.note.apply{
                                    title = state.titleText
                                    color = state.currentNoteColor
                                    lastUpdated = OffsetDateTime.now()
                                }
                                if(isCurrentlyPinned){ //then unpin note
                                    run runBlock@ {
                                        sortedData.forEach rankForLoop@{ rankWithPinnedState ->
                                            if(!rankWithPinnedState.isPinned){ //get first unPinned rank
                                                newRank = rankWithPinnedState.rank - 1L
                                                return@runBlock
                                            }
                                        }
                                    }
                                    updatedNote.apply{
                                        rank = if(newRank == -1L) 0 else newRank
                                        isPinned = false
                                    }
                                }else { //pin note
                                    //increment it so this ensures that note stays at top
                                    newRank = sortedData[0].rank + 1
                                    updatedNote.apply{
                                        rank = newRank
                                        isPinned = true
                                    }
                                }
                                repository.upsertNoteWithTodos(
                                    updatedNote.also { it.lastUpdated = OffsetDateTime.now() },
                                    toDoItems = listOf(
                                        ToDoItem(noteId = state.note.id!!, message = state.plainNoteContent, id = state.plainNoteId)
                                    )
                                )
                                state = state.copy(note = updatedNote, isPinned = !isCurrentlyPinned, isLoading = false)
                            }
                            is Resource.Loading -> {
                                state = state.copy(isLoading = true)
                            }
                            else -> Unit
                        }
                    }.launchIn(viewModelScope)
                }
            }
        }
    }

    private fun isNoteAndTodosEmpty(): Boolean = state.isTitleEmpty && state.plainNoteContent.isBlank()

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}