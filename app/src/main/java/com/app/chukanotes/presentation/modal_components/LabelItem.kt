package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.CheckboxColor

@Composable
fun LabelItem(
    labelTitle: String,
    modifier: Modifier = Modifier,
    onCheckedChanged: (String, Boolean) -> Unit,
    onDeleteLabel: (String) -> Unit,
    isActive: Boolean,
){
    Row(modifier = modifier
        .fillMaxWidth()
        .padding(vertical = 3.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Checkbox(
            checked = isActive,
            onCheckedChange = { checkedState -> onCheckedChanged(labelTitle, checkedState) },
            colors = CheckboxDefaults.colors(
                uncheckedColor = CheckboxColor,
                checkedColor = CheckboxColor,
                checkmarkColor = Color.White
            )
        )
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            text = labelTitle,
            color = AlmostWhiteBackground,
            fontSize = 18.sp,
            modifier = modifier
                .weight(1f)
                .fillMaxWidth()
        )
        //delete button
        IconButton(
            onClick = { onDeleteLabel(labelTitle) },
            modifier = modifier.padding(6.dp))
        {
            Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete", tint = AlmostWhiteBackground)
        }
    }
}