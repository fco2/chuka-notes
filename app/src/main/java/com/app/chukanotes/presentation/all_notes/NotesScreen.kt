package com.app.chukanotes.presentation.all_notes

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.compose.animation.*
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.chukanotes.R
import com.app.chukanotes.core.Constants
import com.app.chukanotes.core.Constants.TO_DO_LIST_NOTE_FORMAT
import com.app.chukanotes.presentation.all_notes.components.NoteWithPreviewItem
import com.app.chukanotes.presentation.all_notes.components.SearchNotesTextField
import com.app.chukanotes.presentation.util.BackPressedHandler
import com.app.chukanotes.presentation.util.Screens
import com.app.chukanotes.presentation.util.SnackBarUtil
import com.app.chukanotes.presentation.util.SnackbarMessageType
import com.app.chukanotes.ui.theme.*
import com.google.accompanist.permissions.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@OptIn(ExperimentalPermissionsApi::class)
class LocalNotePermissionState(
  override val permission: String = "",
  override val status: PermissionStatus = PermissionStatus.Granted
) : PermissionState {
  override fun launchPermissionRequest() {}
}

@OptIn(ExperimentalPermissionsApi::class)
@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@ExperimentalStdlibApi
@ExperimentalFoundationApi
@Composable
fun NotesScreen(
  modifier: Modifier = Modifier,
  viewModel: NotesViewModel = hiltViewModel(),
  navController: NavController,
  notesTitle: String
) {
  val scaffoldState = rememberScaffoldState()
  val coroutineScope = rememberCoroutineScope()
  val context = LocalContext.current
  val isRotated = rememberSaveable { mutableStateOf(false) }
  val areAddNoteAndToDoIconsVisible = rememberSaveable { mutableStateOf(false) }
  val duration = 300
  //Rotate fab
  val angle: Float by animateFloatAsState(
    targetValue = if (isRotated.value) 135f else 0f, animationSpec = tween(
      durationMillis = duration,
      easing = FastOutSlowInEasing
    )
  )

  val onBackAction: () -> Unit = {
    handleBackAction(context)
  }
  val keyboardController = LocalSoftwareKeyboardController.current
  val showKeyboard = remember { mutableStateOf(false) }
  val permissionState: PermissionState =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
      rememberPermissionState(permission = Manifest.permission.POST_NOTIFICATIONS)
    } else {
      LocalNotePermissionState()
    }

  //instantiate back pressed object
  BackPressedHandler(onBackPressed = onBackAction)

  LaunchedEffect(key1 = true) {
    viewModel.uiEvent.collectLatest { event ->
      when (event) {
        is UiEvent.OnNavigate -> {
          navController.navigate(event.route + "?noteId=${event.noteId}") {
            launchSingleTop = true
          }
        }
        is UiEvent.UndoDelete -> {
          SnackBarUtil.showSnackBarByType(
            SnackbarMessageType.Restored,
            snackBarHostState = scaffoldState.snackbarHostState,
            coroutineScope,
            context,
            hasMultipleItems = event.notesToRestoreCount > 1
          )
        }
        is UiEvent.OnDeleteNotes -> {
          SnackBarUtil.showSnackBarByType(
            SnackbarMessageType.Deleted,
            snackBarHostState = scaffoldState.snackbarHostState,
            coroutineScope,
            context,
            undoDeleteEvent = { viewModel.onEvent(NotesEvent.OnUndoDeleteNotes) },
            resetDeletedItemsCache = { viewModel.onEvent(NotesEvent.ResetDeletedItemsCache) },
            hasMultipleItems = event.notesToDeleteCount > 1
          )
        }
        UiEvent.HideSoftKeyboard -> {
          keyboardController?.hide()
        }
        UiEvent.ShowSoftKeyboard -> {
          showKeyboard.value = true
        }
      }
    }
  }

  Scaffold(scaffoldState = scaffoldState, modifier = modifier.fillMaxSize())
  { padding ->
    Box(
      modifier = modifier
        .fillMaxSize()
        .background(NotSoBlack)
        .padding(padding)
    )
    {
      Column(modifier = modifier.fillMaxSize())
      {
        Row(
          modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
            .background(DarkerGray),
          verticalAlignment = Alignment.CenterVertically,
          horizontalArrangement = Arrangement.SpaceBetween
        )
        {
          Row(modifier = modifier.fillMaxHeight(), verticalAlignment = Alignment.CenterVertically)
          {
            if (viewModel.notesToolbarState == NotesToolbarState.Default) {
              //notes title
              Text(
                text = notesTitle,
                fontSize = 26.sp,
                fontWeight = FontWeight.SemiBold,
                color = AlmostWhiteBackground,
                textAlign = TextAlign.Center,
                modifier = modifier.padding(start = 20.dp)
              )
            }
            if (viewModel.notesToolbarState != NotesToolbarState.Default) {
              //close icon
              IconButton(
                onClick = { viewModel.onEvent(NotesEvent.ResetNotesQueuedForDelete) },
                modifier = modifier.fillMaxHeight()
              ) {
                Icon(
                  painter = painterResource(id = R.drawable.baseline_close_white_48),
                  contentDescription = "",
                  modifier = modifier
                    .background(Color.Transparent)
                    .align(Alignment.CenterVertically)
                    .size(34.dp),
                  tint = AlmostWhiteBackground,
                )
              }
            }
            //search textField
            if (viewModel.notesToolbarState == NotesToolbarState.Search) {
              Box(
                modifier = modifier
                  .weight(1f)
                  .fillMaxWidth()
                  .height(40.dp)
                  .align(Alignment.CenterVertically)
                  .padding(5.dp)
              ) {
                SearchNotesTextField(
                  searchTextFieldValue = viewModel.searchTextFieldValue,
                  hintText = context.getString(R.string.search_notes_and_todos),
                  showKeyBoard = showKeyboard.value,
                  onSearchFieldChanged = { newValue ->
                    viewModel.onEvent(NotesEvent.OnSearchFieldChanged(newValue))
                  },
                  onNoteSearchTriggered = { viewModel.onEvent(NotesEvent.OnNoteSearchTriggered) }
                )
              }
            }
          }
          Row(
            modifier = modifier.fillMaxHeight(),
            verticalAlignment = Alignment.CenterVertically
          )
          {
            if (viewModel.notesToolbarState == NotesToolbarState.Default) {
              //search icon
              IconButton(
                onClick = { viewModel.onEvent(NotesEvent.OnShowSearchField) },
                modifier = modifier.fillMaxHeight()
              ) {
                Icon(
                  painter = painterResource(id = R.drawable.baseline_search_white_48),
                  contentDescription = "", modifier = modifier
                    .background(Color.Transparent)
                    .align(Alignment.CenterVertically)
                    .size(34.dp),
                  tint = AlmostWhiteBackground
                )
              }
            }
            if (viewModel.notesToolbarState == NotesToolbarState.DeleteNote) {
              //delete icon
              IconButton(
                onClick = { viewModel.onEvent(NotesEvent.OnDeleteNotes) },
                modifier = modifier.fillMaxHeight()
              ) {
                Icon(
                  painter = painterResource(id = R.drawable.baseline_delete_outline_white_48),
                  contentDescription = "", modifier = modifier
                    .background(Color.Transparent)
                    .align(Alignment.CenterVertically)
                    .size(34.dp),
                  tint = AlmostWhiteBackground
                )
              }
            }
          }
        }
        // list
        if (!viewModel.isLoading) {
          LazyVerticalStaggeredGrid(
            columns = StaggeredGridCells.Fixed(2),
            modifier = Modifier.padding(0.dp)
          ) {
            //list of notes
            items(items = viewModel.notes, key = {note -> note.id}) { noteWithPreview ->
              val route by remember(noteWithPreview.note.noteFormat) {
                derivedStateOf {
                  if (noteWithPreview.note.noteFormat == TO_DO_LIST_NOTE_FORMAT)
                    Screens.TODO_ITEM
                  else
                    Screens.PLAIN_NOTE
                }
              }
              NoteWithPreviewItem(
                noteWithPreview = noteWithPreview,
                viewModel = viewModel,
                onClick = { note ->
                  if (viewModel.notesToolbarState == NotesToolbarState.DeleteNote)
                    viewModel.onEvent(NotesEvent.OnNoteLongPressed(noteWithPreview))
                  else {
                    // Add permission check here
                    if (route == Screens.PLAIN_NOTE) {
                      viewModel.onEvent(
                        NotesEvent.OnNavigate(
                          note.id!!,
                          route
                        )
                      )
                    } else {
                      when (permissionState.status) {
                        // third parameter is to enable this work for api less than 33
                        is PermissionStatus.Granted -> {
                          viewModel.onEvent(
                            NotesEvent.OnNavigate(
                              note.id!!,
                              route
                            )
                          )
                        }
                        is PermissionStatus.Denied -> {
                          //use snackBar to ask user to enable permission
                          //please enable permission in settings [Launch]
                          if (permissionState.status.shouldShowRationale) {
                            coroutineScope.launch {
                              val result = scaffoldState.snackbarHostState
                                .showSnackbar(
                                  context.getString(R.string.enable_notification_reminder_in_settings),
                                  actionLabel = context.getString(R.string.launch_settings)
                                )
                              if (result == SnackbarResult.ActionPerformed) {
                                // Launch Settings
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri = Uri.fromParts("package", context.packageName, null)
                                intent.data = uri
                                context.startActivity(intent)
                              }
                            }
                          } else {
                            coroutineScope.launch {
                              val result = scaffoldState.snackbarHostState
                                .showSnackbar(
                                  context.getString(R.string.enable_notification_reminder),
                                  actionLabel = context.getString(R.string.enable)
                                )
                              if (result == SnackbarResult.ActionPerformed) {
                                permissionState.launchPermissionRequest()
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                },
                onNoteSelected = { noteWithPreviewForSelect ->
                  viewModel.onEvent(NotesEvent.OnNoteLongPressed(noteWithPreviewForSelect))
                },
                coroutineScope = coroutineScope
              )
            }
            item {
              Box(modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 70.dp))
            }
          }
        }
      }
      //Box for 3 fabs
      Column(
        modifier = modifier
          .align(Alignment.BottomEnd)
          .padding(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
      )
      {
        AnimatedVisibility(
          visible = areAddNoteAndToDoIconsVisible.value,
          enter = slideInVertically(
            animationSpec = tween(durationMillis = duration, easing = FastOutSlowInEasing),
            initialOffsetY = { 90 }) + fadeIn(
            initialAlpha = 0f,
            animationSpec = tween(durationMillis = duration)
          ),
          exit = slideOutVertically(animationSpec = tween(
            durationMillis = duration,
            easing = FastOutSlowInEasing
          ),
            targetOffsetY = { 90 }) + fadeOut(
            targetAlpha = 0f,
            animationSpec = tween(durationMillis = duration)
          )
        ) {
          //Column to animate
          Column(horizontalAlignment = Alignment.CenterHorizontally)
          {
            // add to-do list
            Box(modifier = Modifier
              .size(40.dp)
              .background(color = SmallerFabBackground, shape = CircleShape)
              .clickable {
                // Add permission check here
                when (permissionState.status) {
                  is PermissionStatus.Granted -> {
                    viewModel.onEvent(
                      NotesEvent.OnCreateNoteWithToDoItem(
                        format = TO_DO_LIST_NOTE_FORMAT,
                        route = Screens.TODO_ITEM
                      )
                    )
                  }
                  is PermissionStatus.Denied -> {
                    //use snackBar to ask user to enable permission
                    //please enable permission in settings [Launch]
                    if (permissionState.status.shouldShowRationale) {
                      coroutineScope.launch {
                        val result = scaffoldState.snackbarHostState
                          .showSnackbar(
                            context.getString(R.string.enable_notification_reminder_in_settings),
                            actionLabel = context.getString(R.string.launch_settings)
                          )
                        if (result == SnackbarResult.ActionPerformed) {
                          // Launch Settings
                          val intent =
                            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                          val uri =
                            Uri.fromParts("package", context.packageName, null)
                          intent.data = uri
                          context.startActivity(intent)
                        }
                      }
                    } else {
                      coroutineScope.launch {
                        val result = scaffoldState.snackbarHostState
                          .showSnackbar(
                            context.getString(R.string.enable_notification_reminder),
                            actionLabel = context.getString(R.string.enable)
                          )
                        if (result == SnackbarResult.ActionPerformed) {
                          permissionState.launchPermissionRequest()
                        }
                      }
                    }
                  }
                }
                isRotated.value = false
                areAddNoteAndToDoIconsVisible.value = false
              }
            ) {
              Image(
                painter = painterResource(id = R.drawable.add_to_do_list_fab),
                contentDescription = "",
                modifier = Modifier
                  .size(30.dp)
                  .padding(2.dp)
                  .align(Alignment.Center)
              )
            }
            Spacer(modifier = Modifier.height(8.dp))
            // add plain note
            Box(modifier = Modifier
              .size(40.dp)
              .background(color = SmallerFabBackground, shape = CircleShape)
              .clickable {
                viewModel.onEvent(
                  NotesEvent.OnCreateNoteWithToDoItem(
                    format = Constants.PLAIN_TEXT_NOTE_FORMAT,
                    route = Screens.PLAIN_NOTE
                  )
                )
                isRotated.value = false
                areAddNoteAndToDoIconsVisible.value = false
              }) {
              Image(
                painter = painterResource(id = R.drawable.add_note_fab), contentDescription = "",
                modifier = Modifier
                  .size(30.dp)
                  .padding(2.dp)
                  .align(Alignment.Center)
              )
            }
            Spacer(modifier = Modifier.height(8.dp))
          }
        }
        FloatingActionButton(
          onClick = {
            isRotated.value = !isRotated.value
            areAddNoteAndToDoIconsVisible.value = !areAddNoteAndToDoIconsVisible.value
          },
          backgroundColor = FabButtonBackground, contentColor = AlmostWhiteBackground,
        ) {
          Icon(
            imageVector = Icons.Default.Add,
            contentDescription = "",
            modifier = Modifier.rotate(angle)
          )
        }
      }

      if (viewModel.isLoading) {
        CircularProgressIndicator(
          modifier = Modifier.align(Alignment.Center),
          color = AlmostWhiteBackground
        )
      }

      viewModel.notes.let {
        if (!viewModel.isLoading && viewModel.notes.isEmpty() && viewModel.notesToolbarState != NotesToolbarState.Search) {
          Text(
            text = context.getString(R.string.add_a_note_or_todo_placeholder),
            Modifier.align(Alignment.Center),
            color = NotSoBlackLightHue,
            fontSize = 20.sp,
            fontWeight = FontWeight.SemiBold,
            fontFamily = FontFamily.Serif
          )
        }
      }
    }
  }
}

private fun handleBackAction(context: Context) {
  val activity = context as? Activity
  activity?.finish()
}