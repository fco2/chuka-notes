package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.DarkerGray
import com.app.chukanotes.ui.theme.InactiveTextColor

@Composable
fun SheetContent(
    modifier: Modifier,
    currentNoteColor: Long,
    currentNoteLabel: String,
    labelList: List<String>,
    modalType: ModalType,
    modalTitle: String = "",
    titleHintText: String,
    onColorSelected: (Long) -> Unit,
    onLabelTextFieldChanged: (String) -> Unit,
    onLabelCheckboxChanged: (String, Boolean) -> Unit,
    onDeleteLabel: (String) -> Unit,
    currentEditingLabelText: String,
    onSaveNewLabel: () -> Unit
) {
    Column(
        modifier = modifier
            .background(DarkerGray)
            .fillMaxWidth()
            .fillMaxHeight(0.8f)
            .padding(1.dp)
            .padding(8.dp))
    {
        //BottomSheet modal handle
        Box(
            modifier = modifier
                .align(Alignment.CenterHorizontally)
                .height(2.dp)
                .fillMaxWidth(0.35f)
                .background(InactiveTextColor)
                .padding(8.dp)
        )
        //conditionally show the correct list
        if (modalType == ModalType.Color) {
            Text(
                text = modalTitle,
                modifier = modifier
                    .fillMaxWidth()
                    .padding(10.dp)
                    .padding(5.dp),
                textAlign = TextAlign.Center,
                fontSize = 18.sp, fontWeight = FontWeight.Bold, color = AlmostWhiteBackground
            )
            ColorList(
                modifier = modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                currentNoteColor = currentNoteColor,
                onColorSelected = { selectedColor -> onColorSelected(selectedColor) }
            )
        } else {
            //label textField
            Box(
                modifier = modifier
                    .fillMaxWidth()
                    .padding(10.dp)
            )
            {
                AddLabelTextField(
                    currentEditingLabelText = currentEditingLabelText,
                    hintText = titleHintText,
                    onLabelTextFieldChanged = { newValue -> onLabelTextFieldChanged(newValue) },
                    onSaveNewLabel = { onSaveNewLabel() }
                )
            }
            //label checkbox list
            LabelList(
                modifier = modifier,
                list = labelList,
                currentNoteLabel = currentNoteLabel,
                onCheckedChanged = { text, checked -> onLabelCheckboxChanged(text, checked) },
                onDeleteLabel = { label -> onDeleteLabel(label) }
            )
        }
    }
}

enum class ModalType {
    Color,
    Label,
    None
}