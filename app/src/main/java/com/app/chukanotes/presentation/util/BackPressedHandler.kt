package com.app.chukanotes.presentation.util

import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalLifecycleOwner

@Composable
fun BackPressedHandler(
    onBackPressed: () -> Unit,
    enabled: Boolean = true
){
    val backPressedDispatcher = checkNotNull(LocalOnBackPressedDispatcherOwner.current) {
        "No OnBackPressedDispatcherOwner was provided via LocalOnBackPressedDispatcherOwner"
    }.onBackPressedDispatcher

    val currentOnBackPressed by rememberUpdatedState(newValue = onBackPressed)

    val backPressedCallback = remember {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                currentOnBackPressed()
            }
        }
    }
    // On every successful composition, update the callback with the `enabled` value
    SideEffect {
        backPressedCallback.isEnabled = enabled
    }
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(lifecycleOwner, backPressedDispatcher,){
        backPressedDispatcher.addCallback(lifecycleOwner, backPressedCallback)

        onDispose {
            backPressedCallback.remove()
        }
    }
}