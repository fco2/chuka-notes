package com.app.chukanotes.presentation.todo_list

import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.Note

sealed class ToDoEvent{
    data class OnCheckedChanged(val toDoItem: ToDoItem, val checked: Boolean): ToDoEvent()
    data class OnDeleteToDo(val toDoItem: ToDoItem): ToDoEvent()
    object UnDoDelete: ToDoEvent()
    object OnAddToDo: ToDoEvent()
    data class OnTextFieldValueChanged(val toDoItem: ToDoItem, val newValue: String): ToDoEvent()
    object OnBackPressed: ToDoEvent()
    data class OnTitleFieldValueChanged(val newValue: String): ToDoEvent()
    data class OnNoteColorChanged(val note: Note, val newColor: Long): ToDoEvent()
    //These should be in their own space
    data class OnLabelTextFieldChanged(val labelText: String): ToDoEvent()
    data class OnLabelCheckboxChecked(val labelText: String, val isChecked: Boolean): ToDoEvent()
    object OnLabelModalOpened: ToDoEvent()
    data class OnDeleteLabel(val labelTitle: String): ToDoEvent()
    object OnSaveNewLabel: ToDoEvent()
    object OnDeleteNote: ToDoEvent()
    object OnPinnedStateToggle: ToDoEvent()
}
