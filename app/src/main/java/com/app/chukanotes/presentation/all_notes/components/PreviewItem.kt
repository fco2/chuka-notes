package com.app.chukanotes.presentation.all_notes.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.core.Constants.TO_DO_LIST_NOTE_FORMAT
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.InactiveTextColor

@Composable
fun PreviewItem(
    itemToPreview: String,
    noteFormat: String,
    modifier: Modifier = Modifier,
    openNoteOrToDo: () -> Unit
){
    val isToDoNoteFormat = noteFormat == TO_DO_LIST_NOTE_FORMAT
    Row(modifier = modifier
        .fillMaxWidth()
        .padding(3.dp), verticalAlignment = Alignment.CenterVertically) {
        if(isToDoNoteFormat){ // for to-do lists
            Checkbox(
                checked = false,
               // enabled = false,
                onCheckedChange = { openNoteOrToDo() },
                colors = CheckboxDefaults.colors(uncheckedColor = InactiveTextColor) ,
                modifier = modifier
                    .padding(5.dp)
                    .height(14.dp)
                    .weight(0.1F),
            )
        }
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            text = itemToPreview,
            fontSize = 14.sp,
            color = AlmostWhiteBackground,
            maxLines = 6,
            overflow = TextOverflow.Ellipsis,
            modifier = modifier
                .weight(0.9f)
                .fillMaxWidth()
        )
    }
}