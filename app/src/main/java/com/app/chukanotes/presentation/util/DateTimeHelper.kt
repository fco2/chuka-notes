package com.app.chukanotes.presentation.util

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class DateTimeHelper {
    companion object{
        fun getLastUpdatedDate(odt: OffsetDateTime?): String {
            if (odt == null) return ""
            val minute = if (odt.minute < 10) "0${odt.minute}" else "${odt.minute}"
            val hour = if (odt.hour < 10) "0${odt.hour}" else "${odt.hour}"
            return "Updated ${odt.dayOfMonth}${getDayOfMonthSuffix(odt.dayOfMonth)} " +
                "${
                    odt.month.name.substring(0, 3).lowercase(Locale.getDefault())
                        .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
                }, " +
                "${odt.year.toString().substring(2, 4)}' at ${hour}:${minute}"
        }

        private fun getDayOfMonthSuffix(n: Int): String {
            return if (n in 11..13) {
                "th"
            } else when (n % 10) {
                1 -> "st"
                2 -> "nd"
                3 -> "rd"
                else -> "th"
            }
        }

        fun toOffsetDateTime(date: String?): OffsetDateTime?{
            val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
            return date?.let {
                return formatter.parse(date, OffsetDateTime::from)
            }
        }

        fun getStringifiedDate(lastUpdated: OffsetDateTime): String?{
            val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
            return lastUpdated.format(formatter)
        }
    }
}