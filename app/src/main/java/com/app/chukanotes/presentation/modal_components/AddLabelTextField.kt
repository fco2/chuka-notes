package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.InactiveTextColor

@Composable
fun AddLabelTextField(
    modifier: Modifier = Modifier,
    currentEditingLabelText: String,
    hintText: String,
    onLabelTextFieldChanged: (String) -> Unit,
    onSaveNewLabel: () -> Unit
){
    Box(modifier = modifier.fillMaxWidth()){
        Box(modifier = Modifier
            .fillMaxWidth()
            .height(1.dp)
            .background(
                AlmostWhiteBackground
            )
            .align(Alignment.BottomCenter)
            .padding(5.dp))
        BasicTextField(
            value = currentEditingLabelText,
            onValueChange = {newValue -> onLabelTextFieldChanged(newValue) },
            textStyle = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight.Normal,
                color = AlmostWhiteBackground,
            ),
            modifier = Modifier.fillMaxWidth().padding(5.dp),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
                capitalization = KeyboardCapitalization.Sentences
            ),
            keyboardActions = KeyboardActions( onDone = {
                onSaveNewLabel()
            }),
            cursorBrush = SolidColor(Color.Green)
        )
        if(currentEditingLabelText.isEmpty()) {
            Text(
                text = hintText,
                fontSize = 18.sp,
                fontWeight = FontWeight.Normal,
                color = InactiveTextColor,
                modifier = Modifier.fillMaxWidth().padding(start = 6.dp)
            )
        }
    }
}