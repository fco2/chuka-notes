package com.app.chukanotes.presentation.util

import java.util.*

object NotificationCalendar {
    //sunday = 1, monday = 2
    private val daysMap = mapOf(
        "tomorrow" to 0,
        "sunday" to Calendar.SUNDAY,
        "monday" to Calendar.MONDAY,
        "tuesday" to Calendar.TUESDAY,
        "wednesday" to Calendar.WEDNESDAY,
        "thursday" to Calendar.THURSDAY,
        "friday" to Calendar.FRIDAY,
        "saturday" to Calendar.SATURDAY,
        "sun" to Calendar.SUNDAY,
        "mon" to Calendar.MONDAY,
        "tue" to Calendar.TUESDAY,
        "wed" to Calendar.WEDNESDAY,
        "thur" to Calendar.THURSDAY,
        "fri" to Calendar.FRIDAY,
        "sat" to Calendar.SATURDAY,
    )

    fun getDateTimeToNotify(text: String): Calendar? {
        val wordsList = text.replace('@', ' ').lowercase().split(" ")
        val stringAfterAtSymbol = text.substringAfter('@')
        val cal: Calendar? = Calendar.getInstance()
        var dayOfWeek = cal!!.get(Calendar.DAY_OF_WEEK) //initialized to today

        run runBlock@ { //iterate from behind so we get most accurate day
            wordsList.asReversed().forEach { word ->
                if(daysMap.keys.contains(word)){
                    dayOfWeek = daysMap[word]!!
                    return@runBlock
                }
            }
        }
        val currentMonthSize = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        when  {// for tomorrow date who's today date is not end of month
            dayOfWeek == 0 && currentMonthSize != cal.get(Calendar.DAY_OF_MONTH) -> {
                cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1) //if tomorrow is chosen
            }
            dayOfWeek == 0 && currentMonthSize == cal.get(Calendar.DAY_OF_MONTH) -> { // today is last day of month, so set tomorrow to the first. take year into account
                cal.set(Calendar.DATE, 1)
                if(cal.get(Calendar.YEAR) == 11){ //set to January 1st next year
                    cal.set(Calendar.MONTH, 0)
                    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1)
                }else{
                    cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1)
                }
            }
            else -> {
                //if we try to set day of the week for day previous to today, we need to set reminder for same day next week.
                // Hence why we set day of week to that day this week and add a week(7 days) to get same day next week
                if(cal.get(Calendar.DAY_OF_WEEK) > dayOfWeek) {
                    val newDate = getNewFutureDate(dayOfWeek)
                    cal.set(Calendar.DATE, newDate)
                }else
                    cal.set(Calendar.DAY_OF_WEEK, dayOfWeek)
            }
        }
        return setHourAndMinute(stringAfterAtSymbol, cal, wordsList)
    }

    private fun setHourAndMinute(
        dateTimeSubStr: String,
        cal: Calendar,
        wordsList: List<String>
    ): Calendar?{
        var hour: Int = -1
        //account for midnight and noon
        if(wordsList.contains("noon")){
            cal.set(Calendar.HOUR, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.AM_PM, Calendar.PM)
        }else if(wordsList.contains("midnight")){
            cal.set(Calendar.HOUR, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.AM_PM, Calendar.AM)
            //consider end of month and end of year, since we are setting day to tomorrow
            val currentMonthSize = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            if(currentMonthSize == cal.get(Calendar.DAY_OF_MONTH) && cal.get(Calendar.MONTH) == 11){
                // set to 1st of next year
                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1)
                cal.set(Calendar.MONTH, 0) // set to January 1st next year
                cal.set(Calendar.DAY_OF_MONTH, 1)
            }else if(currentMonthSize == cal.get(Calendar.DAY_OF_MONTH) && cal.get(Calendar.MONTH) != 11){
                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1)
                cal.set(Calendar.DAY_OF_MONTH, 1)
            }else {
                cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1) // set date to tomorrow
            }
        }else if(dateTimeSubStr.contains(':')) { // it's : format
            val index = dateTimeSubStr.indexOf(':')
            val potentialHour = dateTimeSubStr.substring(0, index).trim()
            val potentialMinute = dateTimeSubStr.substring(index + 1)
            hour = getFirstDigits(potentialHour)
            val minute = getFirstDigits(potentialMinute)

            if(hour == -1 || minute == -1) //improperly formatted date
                return null

            cal.apply {
                set(Calendar.HOUR, hour)
                set(Calendar.MINUTE, if(minute == 0) 1 else minute)
            }
            //To set AM or PM
            setAmOrPm(dateTimeSubStr, cal)
        } else{
            hour = getFirstDigits(dateTimeSubStr)
            if(hour == -1) return null
            cal.set(Calendar.HOUR, hour)
            cal.set(Calendar.MINUTE, 0)
            //To set AM or PM
            setAmOrPm(dateTimeSubStr, cal)
        }
        cal.set(Calendar.SECOND, 1)
        // 12 noon and 12 midnight anomalous behaviour. They are swapped in Calendar library
        //so just swap them here to be the correct one
        if(hour == 12){
            cal.set(Calendar.HOUR, 0)
            //To set AM or PM
            setAmOrPm(dateTimeSubStr, cal)
        }
        /*val s = "CHUKA-note -> getBaseImplementation: --||$dateTimeSubStr||hour: ${cal.get(Calendar.HOUR)} " +
                "||min: ${cal.get(Calendar.MINUTE)} ||date: ${cal.get(Calendar.DATE)} ||month: ${cal.get(Calendar.MONTH)} " +
                "|| ${cal.get(Calendar.YEAR)}||time: ${cal.time} ||milli time:  ${cal.timeInMillis} || "
        Timber.d(s)*/

        //check if time has passed, if day is same, return null else add 7 to day of week to set future date alarm
        val todayDate = Calendar.getInstance()
        if(cal.timeInMillis <= todayDate.timeInMillis){ // means date has passed, so return null
            //Timber.d("CHUKA-note..sorry I'm NULL, cal.timeInMillis: ${cal.timeInMillis} | ${todayDate.timeInMillis}")
                return null
        }
        return cal
    }

    private fun setAmOrPm(dateTimeSubStr: String, cal: Calendar) {
        if (dateTimeSubStr.contains("pm", true))
            cal.set(Calendar.AM_PM, Calendar.PM)
        else
            cal.set(Calendar.AM_PM, Calendar.AM)
    }
    //getBaseImplementation: --|| 8am tomorrow||hour: 8 ||min: 0 ||date: 13 ||month: 0 || 2021 ||time: Wed Jan 13 08:00:00 EST 2021 ||milli time:  1610542800161

    private fun getFirstDigits(s: String): Int{
        //length of 0
        if(s.isEmpty()) return -1
        //length of 1
        if(s.length == 1 && s[0].isDigit()) return s.toInt()
        //handle digit length of greater than 1
        //always only returns first two digits as it will only every be XX:XX
        val sb = StringBuilder()
        for(index in s.indices){
            if(s[index].isDigit()){
                sb.append(s[index])
                if(index + 1 < s.length && s[index + 1].isDigit())
                    sb.append(s[index + 1])
                //Timber.d("CHUKA-note: getFirstDigits: ${sb.toString().toInt()}")
                return sb.toString().toInt()
            }
        }
        return -1
    }

    private fun getNewFutureDate(dayOfWeek: Int): Int {
        val tempCal = Calendar.getInstance()
        tempCal.set(Calendar.DAY_OF_WEEK, dayOfWeek)
        return tempCal.get(Calendar.DATE) + 7
    }
}

/*
// use flag to set proper format of date/time, so if was not set, return null.
Acceptable date time formats
@ 7:59am
@7am
@9  --> this will translate to AM
@11pm on Sunday
on Monday @11:05am
@ 13:00 tue
@10am tomorrow
@ midnight -> This translates to 00:00 of the following day
@ noon -> this translates to 12 noon
 */
