package com.app.chukanotes.presentation.plain_note

import com.app.chukanotes.domain.models.Note

data class PlainNoteState(
    val isTitleEmpty: Boolean = false,
    val titleText: String = "",
    val note: Note = Note(),
    val plainNoteContent: String = "",
    val isScreenContentInitialized: Boolean = false,
    val isLoading: Boolean = false,
    val currentNoteColor: Long = 0xff263238,
    val currentNoteLabel: String = "",
    val currentEditingLabelText: String = "",
    val isPinned: Boolean = false,
    val plainNoteId: Long = -1L
)
