package com.app.chukanotes.presentation.todo_list.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.CheckboxColor
import com.app.chukanotes.ui.theme.InactiveTextColor

@Composable
fun ToDoItemCompose(
    toDoItem: ToDoItem,
    text: String,
    isCompleted: Boolean,
    modifier: Modifier = Modifier,
    onCheckedChanged: (ToDoItem, Boolean) -> Unit,
    onDeleteToDo: (ToDoItem) -> Unit,
    onTextFieldChanged: (ToDoItem, String) -> Unit
){
    Row(modifier = modifier
        .fillMaxWidth()
        .padding(vertical = 3.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Checkbox(
            checked = isCompleted,
            onCheckedChange = { checked -> onCheckedChanged(toDoItem, checked) },
            colors = CheckboxDefaults.colors(uncheckedColor = CheckboxColor,
                checkedColor = if(isCompleted) InactiveTextColor else CheckboxColor, checkmarkColor = Color.White) ,
        )
        Spacer(modifier = Modifier.width(6.dp))
        BasicToDoTextField(
            toDoItem = toDoItem,
            text = text,
            isCompleted = isCompleted,
            modifier = modifier
                .weight(1f)
                .fillMaxWidth(),
            onTextFieldChanged = { toDo, newValue -> onTextFieldChanged(toDo, newValue) }
        )
        //delete button
        IconButton(
            onClick = { onDeleteToDo(toDoItem) },
            modifier = modifier.padding(6.dp))
        {
            Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete",
                tint = if(isCompleted) InactiveTextColor else AlmostWhiteBackground)
        }
    }
}