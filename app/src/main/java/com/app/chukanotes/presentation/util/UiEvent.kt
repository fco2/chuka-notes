package com.app.chukanotes.presentation.util

sealed class UiEvent{
    data class OnBackPressed(
        val noteId: Long = -1L,
        val message: String = "",
        val toDoId: Long = -1L,
        val dateTimeInMillis: Long = -1L
    ): UiEvent()
    data class ShowSnackbar(val messageType: SnackbarMessageType, val toDoId: Long? = null): UiEvent()
    object UnDoDelete: UiEvent()
    object OnLabelModalOpened: UiEvent()
    object OnNavigateToNotesScreen: UiEvent()
    data class SetToDoAlarm(
        val noteId: Long,
        val message: String,
        val toDoId: Long,
        val dateTimeInMillis: Long
    ): UiEvent()
    data class CancelAlarm(val toDoId: Long): UiEvent()
}
