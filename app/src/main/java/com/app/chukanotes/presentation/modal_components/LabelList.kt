package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun LabelList(
    modifier: Modifier = Modifier,
    list: List<String>,
    currentNoteLabel: String,
    onCheckedChanged: (String, Boolean) -> Unit,
    onDeleteLabel: (String) -> Unit,
){
    LazyColumn(modifier = modifier
        .fillMaxWidth()
        .fillMaxHeight()
        .padding(5.dp)){

        items(items = list, key = { it }){ labelItem ->
            LabelItem(
                labelTitle = labelItem,
                isActive = currentNoteLabel == labelItem,
                onCheckedChanged = { labelName, checkedState -> onCheckedChanged(labelName, checkedState)},
                onDeleteLabel = { label -> onDeleteLabel(label) }
            )
        }
    }
}