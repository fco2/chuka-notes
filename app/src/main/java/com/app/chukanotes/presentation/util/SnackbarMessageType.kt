package com.app.chukanotes.presentation.util

enum class SnackbarMessageType {
    Deleted,
    Restored
}