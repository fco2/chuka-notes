package com.app.chukanotes.presentation.todo_list

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.chukanotes.R
import com.app.chukanotes.presentation.modal_components.ModalType
import com.app.chukanotes.presentation.modal_components.SheetContent
import com.app.chukanotes.presentation.todo_list.components.ToDoListWithTitle
import com.app.chukanotes.presentation.util.*
import com.app.chukanotes.presentation.util.SnackBarUtil.showSnackBarByType
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.DarkerGray
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@ExperimentalComposeUiApi
@ExperimentalAnimationApi
@ExperimentalStdlibApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun ToDoListScreen(
  modifier: Modifier = Modifier,
  viewModel: ToDoListViewModel = hiltViewModel(),
  navController: NavController
) {
  val scaffoldState = rememberScaffoldState()
  val context = LocalContext.current
  val coroutineScope: CoroutineScope = rememberCoroutineScope()
  val modalBottomSheetState = rememberModalBottomSheetState(
    initialValue = ModalBottomSheetValue.Hidden,
    animationSpec = tween(durationMillis = 300, easing = FastOutSlowInEasing)
  )
  var modalType by rememberSaveable { mutableStateOf(ModalType.None) }
  var modalTitle by rememberSaveable { mutableStateOf("") }
  var shouldConfirmDeleteNote by rememberSaveable { mutableStateOf(false) }
  val focusManager = LocalFocusManager.current

  //do on backPressed action
  val onBackAction: () -> Unit = {
    if (!modalBottomSheetState.isVisible) {
      focusManager.clearFocus()
      handleBackAction(viewModel)
    } else
      coroutineScope.launch(Dispatchers.Main) { modalBottomSheetState.hide() }
  }
  //instantiate back pressed object
  BackPressedHandler(onBackPressed = onBackAction)

  LaunchedEffect(key1 = true) {
    viewModel.uiEvent.collectLatest { event ->
      when (event) {
        is UiEvent.OnBackPressed -> {
          if (event.toDoId != -1L)
            setUpAlarm(context, event.message, event.noteId, event.toDoId, event.dateTimeInMillis)
          navController.navigate(Screens.ALL_NOTES) {
            this.restoreState = true
            launchSingleTop = true
          }
        }
        is UiEvent.ShowSnackbar -> {
          showSnackBarByType(
            SnackbarMessageType.Deleted,
            snackBarHostState = scaffoldState.snackbarHostState,
            coroutineScope,
            context,
            undoDeleteEvent = { viewModel.onEvent(ToDoEvent.UnDoDelete) }
          )
          if (event.toDoId != null)
            cancelAlarm(event.toDoId, context)
        }
        is UiEvent.UnDoDelete -> {
          showSnackBarByType(
            SnackbarMessageType.Restored,
            snackBarHostState = scaffoldState.snackbarHostState,
            coroutineScope,
            context,
          )
        }
        UiEvent.OnLabelModalOpened -> {
          modalType = ModalType.Label
          focusManager.clearFocus(true)
          coroutineScope.launch { modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded) }
        }
        UiEvent.OnNavigateToNotesScreen -> {
          navController.navigate(Screens.ALL_NOTES) {
            launchSingleTop = true
          }
        }
        is UiEvent.SetToDoAlarm -> {
          setUpAlarm(context, event.message, event.noteId, event.toDoId, event.dateTimeInMillis)
        }
        is UiEvent.CancelAlarm -> {
          cancelAlarm(event.toDoId, context)
        }
      }
    }
  }

  ModalBottomSheetLayout(
    sheetState = modalBottomSheetState,
    sheetContentColor = Color.Transparent,
    sheetBackgroundColor = Color.Transparent,
    sheetContent = {
      //bottom sheet modal for colors and labels
      SheetContent(
        modifier = modifier,
        modalType = modalType,
        modalTitle = modalTitle,
        currentNoteColor = viewModel.toDoListState.currentNoteColor,
        currentNoteLabel = viewModel.toDoListState.currentNoteLabel,
        currentEditingLabelText = viewModel.toDoListState.currentEditingLabelText,
        labelList = viewModel.labelList.toList(),
        onColorSelected = { selectedColor ->
          viewModel.onEvent(
            ToDoEvent.OnNoteColorChanged(
              viewModel.toDoListState.note,
              selectedColor
            )
          )
        },
        onLabelCheckboxChanged = { labelText, checked ->
          viewModel.onEvent(ToDoEvent.OnLabelCheckboxChecked(labelText, checked))
        },
        onLabelTextFieldChanged = { newValue ->
          viewModel.onEvent(
            ToDoEvent.OnLabelTextFieldChanged(
              newValue
            )
          )
        },
        onDeleteLabel = { labelTitle -> viewModel.onEvent(ToDoEvent.OnDeleteLabel(labelTitle)) },
        onSaveNewLabel = { viewModel.onEvent(ToDoEvent.OnSaveNewLabel) },
        titleHintText = context.getString(R.string.add_label)
      )
    },
    modifier = Modifier.fillMaxWidth(),
    sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
  ) {
    ToDoListScreenContent(
      modifier = modifier,
      viewModel = viewModel,
      scaffoldState = scaffoldState,
      context = context,
      shouldConfirmDeleteNote = shouldConfirmDeleteNote,
      coroutineScope = coroutineScope,
      modalBottomSheetState = modalBottomSheetState,
      onSetModalTypeAndTitle = { type, title ->
        modalType = type
        modalTitle = title
      },
      onSetShouldConfirmDeleteNote = { flag -> shouldConfirmDeleteNote = flag },
      onConfirmDeleteNote = { newValue -> shouldConfirmDeleteNote = newValue }
    )
  }
}


@ExperimentalMaterialApi
@Composable
fun ToDoListScreenContent(
  modifier: Modifier,
  viewModel: ToDoListViewModel,
  scaffoldState: ScaffoldState,
  context: Context,
  shouldConfirmDeleteNote: Boolean,
  coroutineScope: CoroutineScope,
  modalBottomSheetState: ModalBottomSheetState,
  onSetModalTypeAndTitle: (ModalType, String) -> Unit,
  onSetShouldConfirmDeleteNote: (Boolean) -> Unit,
  onConfirmDeleteNote: (Boolean) -> Unit
) {
  val interactionSource = remember { MutableInteractionSource() }
  val focusManager = LocalFocusManager.current

  Scaffold(scaffoldState = scaffoldState, modifier = modifier.fillMaxSize())
  { padding ->
    Box(
      modifier = modifier
        .fillMaxSize()
        .background(Color(viewModel.toDoListState.currentNoteColor))
        .padding(paddingValues = padding)
    )
    {
      Column(
        modifier = Modifier
          .fillMaxSize()
          .padding(top = 64.dp, start = 5.dp, end = 5.dp, bottom = 5.dp)
      )
      {
        //to-do lists
        ToDoListWithTitle(
          list = viewModel.toDos,
          noteColor = viewModel.toDoListState.currentNoteColor,
          titleText = viewModel.toDoListState.titleText,
          isTitleEmpty = viewModel.toDoListState.isTitleEmpty,
          titleHint = context.getString(R.string.enter_title),
          addToDoIconId = R.drawable.add_icon,
          addIconContentDescription = context.getString(R.string.add_todo_item),
          addToDoHint = context.getString(R.string.add_todo_item),
          onCheckedChanged = { toDo, checked ->
            viewModel.onEvent(
              ToDoEvent.OnCheckedChanged(
                toDo,
                checked
              )
            )
            //hide focus here
            if (checked) {
              focusManager.clearFocus()
            }
          },
          onDeleteToDo = { toDoItem -> viewModel.onEvent(ToDoEvent.OnDeleteToDo(toDoItem)) },
          onTextFieldChanged = { toDo, newValue ->
            viewModel.onEvent(
              ToDoEvent.OnTextFieldValueChanged(
                toDo,
                newValue
              )
            )
          },
          onTitleFieldValueChanged = { newValue ->
            viewModel.onEvent(
              ToDoEvent.OnTitleFieldValueChanged(
                newValue
              )
            )
          },
          onAddToDoItem = { viewModel.onEvent(ToDoEvent.OnAddToDo) }
        )
      }
      //This has to remain at the bottom so that the todos scroll directly under the toolbar
      Row(
        modifier = modifier
          .fillMaxWidth()
          .height(64.dp)
          .padding(0.dp)
          .background(DarkerGray)
          .padding(10.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.SpaceBetween
      )
      {
        //delete icon
        Image(
          painter = painterResource(id = R.drawable.trash_64),
          contentDescription = context.getString(R.string.choose_label),
          modifier = modifier
            .padding(horizontal = 10.dp, vertical = 8.dp)
            .size(40.dp)
            .clickable(
              interactionSource = interactionSource,
              indication = null
            ) { onSetShouldConfirmDeleteNote(true) }
        )

        Row(
          modifier = modifier
            .height(64.dp)
            .padding(0.dp),
          verticalAlignment = Alignment.CenterVertically,
          horizontalArrangement = Arrangement.SpaceEvenly
        )
        {
          Image(
            painter = painterResource(
              id = if (viewModel.toDoListState.isPinned) R.drawable.pinned_icon
              else R.drawable.unpinned_icon
            ),
            contentDescription = context.getString(R.string.pin_note_or_todo),
            modifier = modifier
              .padding(horizontal = 12.dp, vertical = 8.dp)
              .clickable(
                interactionSource = interactionSource,
                indication = null
              ) {
                viewModel.onEvent(ToDoEvent.OnPinnedStateToggle)
              }
          )
          Image(
            painter = painterResource(id = R.drawable.tag),
            contentDescription = context.getString(R.string.choose_label),
            modifier = modifier
              .padding(horizontal = 12.dp, vertical = 8.dp)
              .clickable(
                interactionSource = interactionSource,
                indication = null
              ) {
                viewModel.onEvent(ToDoEvent.OnLabelModalOpened)
              }
          )
          Image(
            painter = painterResource(id = R.drawable.color_picker_124),
            contentDescription = context.getString(R.string.choose_color),
            modifier = modifier
              .padding(horizontal = 12.dp, vertical = 8.dp)
              .clickable(
                interactionSource = interactionSource,
                indication = null
              ) {
                //set modal type and title
                onSetModalTypeAndTitle(
                  ModalType.Color,
                  context.getString(R.string.select_note_background_color)
                )
                focusManager.clearFocus(true)
                coroutineScope.launch {
                  modalBottomSheetState.animateTo(
                    ModalBottomSheetValue.Expanded
                  )
                }
              }
          )
          // TDOD: testing
          val stringToShare = StringBuilder()
          //append title
          if (!viewModel.toDoListState.isTitleEmpty) stringToShare.append("*${viewModel.toDoListState.titleText}*\n")
          // append content
          viewModel.toDos.sortedBy { it.isCompleted }.forEach {
            if (it.isCompleted) stringToShare.append("[x] ") else stringToShare.append("[-] ")
            stringToShare.append("${it.message}\n")
          }
          if (!viewModel.toDoListState.isTitleEmpty || viewModel.toDos.isNotEmpty()) {
            ShareItemAsPlainText(
              context,
              modifier,
              interactionSource,
              stringToShare,
              context.getString(R.string.share_to_do_list)
            )
          }
        }
      }
      //Confirm delete modal
      if (shouldConfirmDeleteNote) {
        focusManager.clearFocus(true)
        Box(modifier = Modifier
          .fillMaxSize()
          .background(Color.Black.copy(alpha = 0.5f))
          .clickable(enabled = false, onClick = {})
        ) {
          ConfirmDeleteNoteModal(
            modifier,
            context,
            shouldConfirmDeleteNote,
            viewModel.toDoListState.currentNoteColor,
            onDeleteNote = { viewModel.onEvent(ToDoEvent.OnDeleteNote) },
            onConfirmDeleteNote = { newValue -> onConfirmDeleteNote(newValue) }
          )
        }
      }
      //Circular progress bar
      if (viewModel.toDoListState.isLoading) {
        CircularProgressIndicator(
          modifier = Modifier.align(Alignment.Center),
          color = AlmostWhiteBackground
        )
      }
    }
  }
}

@ExperimentalComposeUiApi
@ExperimentalStdlibApi
@ExperimentalAnimationApi
@ExperimentalMaterialApi
@ExperimentalFoundationApi
private fun setUpAlarm(
  context: Context,
  alarmMessage: String,
  noteId: Long,
  toDoId: Long,
  timeInMillis: Long
) {
  val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
  val intent = Intent(context, CustomAlarm::class.java)
  intent.putExtra("alarm_message", alarmMessage)
  intent.putExtra("toDoItemId", toDoId)
  intent.putExtra("noteId", noteId)

  val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
    PendingIntent.getBroadcast(
      context,
      toDoId.toInt(),
      intent,
      PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
    )
  else
    PendingIntent.getBroadcast(
      context,
      toDoId.toInt(),
      intent,
      PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    )
  alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent)
}

@ExperimentalComposeUiApi
@ExperimentalStdlibApi
@ExperimentalMaterialApi
@ExperimentalFoundationApi
@ExperimentalAnimationApi
private fun cancelAlarm(toDoId: Long?, context: Context) {
  val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
  val intent = Intent(context, CustomAlarm::class.java)
  val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
    PendingIntent.getBroadcast(
      context,
      toDoId!!.toInt(),
      intent,
      PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
    )
  else
    PendingIntent.getBroadcast(
      context,
      toDoId!!.toInt(),
      intent,
      PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    )
  alarmManager.cancel(pendingIntent)
}

private fun handleBackAction(viewModel: ToDoListViewModel) {
  viewModel.onEvent(ToDoEvent.OnBackPressed)
}