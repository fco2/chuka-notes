package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.app.chukanotes.ui.theme.*

@Composable
fun ColorList(
    modifier: Modifier = Modifier,
    currentNoteColor: Long,
    onColorSelected: (Long) -> Unit
) {
    LazyColumn(modifier = modifier.padding(10.dp).padding(bottom = 40.dp)){
        items(items = getSelectableColors(), key = { it.colorName }){ colorItem ->
            ColorItem(
                colorName = colorItem.colorName,
                color = colorItem.colorValue,
                currentNoteColor = currentNoteColor,
                onColorSelected = { color -> onColorSelected(color) }
            )
        }
    }
}

fun getSelectableColors(): List<ColorItem> {
    return listOf(
        ColorItem("Grey", ColorGreyLong),
        ColorItem("Green", ColorGreenLong),
        ColorItem("Grayish Green", ColorGrayishGreenLong),
        ColorItem("Blue", ColorBlueLong),
        ColorItem("Grayish Yellow", ColorGrayishYellowLong),
        ColorItem("Light Grey", ColorLightBlueLong),
        ColorItem("Brown", ColorBrownLong),
        ColorItem("Grey Magenta", ColorGreyMagentaLong),
        ColorItem("Light Purple", ColorLightPurpleLong),
        ColorItem("Red", ColorRedLong),
        ColorItem("Yellow", ColorYellowLong)
    ).sortedBy { it.colorName }
}

data class ColorItem(
    val colorName: String,
    val colorValue: Long
)