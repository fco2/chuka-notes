package com.app.chukanotes.presentation.all_notes.components

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.core.Constants.TO_DO_LIST_NOTE_FORMAT
import com.app.chukanotes.domain.models.Note
import com.app.chukanotes.domain.models.NoteWithPreview
import com.app.chukanotes.presentation.all_notes.NotesViewModel
import com.app.chukanotes.presentation.util.DateTimeHelper
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.InactiveTextColor
import com.app.chukanotes.ui.theme.InactiveTextColorLightHue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun NoteWithPreviewItem(
    modifier: Modifier = Modifier,
    noteWithPreview: NoteWithPreview,
    coroutineScope: CoroutineScope,
    onClick: (Note) -> Unit,
    onNoteSelected: (NoteWithPreview) -> Unit,
    viewModel: NotesViewModel
){
    // TODO future performance improvements here
    val selectedBorder = BorderStroke(width = 1.5.dp, color = AlmostWhiteBackground)
    val nonSelectedBorder = BorderStroke(width = 0.dp, color = Color.Transparent)
    val noteBorderScale = remember{ Animatable(initialValue = 1f) }
    val isSelectedValue by remember(viewModel.notesQueuedForDelete.size) {
        val isSelected = viewModel.notesQueuedForDelete.contains(noteWithPreview)
        mutableStateOf(isSelected)
    }
    val borderStroke by remember(isSelectedValue) {
        derivedStateOf { if (isSelectedValue) selectedBorder else nonSelectedBorder }
    }
    val isNonEmptyNoteTitle by remember(noteWithPreview.note.title) {
        derivedStateOf { noteWithPreview.note.title.isNotEmpty() }
    }
    val isNonEmptyPreviewOne by remember(noteWithPreview.previewOne) {
        derivedStateOf { noteWithPreview.previewOne != "" }
    }
    val isNonEmptyPreviewTwo by remember(noteWithPreview.previewTwo) {
        derivedStateOf { noteWithPreview.previewTwo != "" }
    }
    val isNonEmptyPreviewThree by remember(noteWithPreview.previewThree) {
        derivedStateOf { noteWithPreview.previewThree != "" }
    }
    val isNonEmptyLabelName by remember(noteWithPreview.labelName) {
        derivedStateOf { noteWithPreview.labelName != "" }
    }
    val isNonNullLastUpdated by remember(noteWithPreview.note.lastUpdated) {
        derivedStateOf { noteWithPreview.note.lastUpdated != null }
    }

    LaunchedEffect(key1 = isSelectedValue){
        coroutineScope.launch {
            noteBorderScale.animateTo(targetValue = 0.9f, animationSpec = tween(durationMillis = 50))
            noteBorderScale.animateTo(targetValue = 1f,
                animationSpec = spring(dampingRatio = Spring.DampingRatioLowBouncy, stiffness = Spring.StiffnessLow)
            )
        }
    }

    Surface(
        elevation = 10.dp,
        shape = RoundedCornerShape(8.dp),
        color = Color(noteWithPreview.note.color),
        modifier = Modifier
            .pointerInput(true) {
                detectTapGestures(
                    onLongPress = {
                        onNoteSelected(noteWithPreview) /* Long Press Detected */
                    },
                    onTap = { onClick(noteWithPreview.note) /* Tap Detected */ }
                )
            }
            .padding(4.dp)
    ) {
        Column(modifier = Modifier
            .clip(RoundedCornerShape(10.dp))
            .background(Color.Transparent)
            .border(
                borderStroke,
                shape = RoundedCornerShape(10.dp)
            )
            .padding(8.dp)
            .scale(noteBorderScale.value)
        ) {
            if(isNonEmptyNoteTitle){
                Text(
                    text = noteWithPreview.note.title,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp,
                    color = AlmostWhiteBackground,
                    modifier = modifier.align(Alignment.CenterHorizontally)
                )
                Spacer(modifier = Modifier.height(10.dp))
            }
            Column(modifier = modifier
                .fillMaxWidth(),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally) {
                //preview 1
                // TODO: previews can be stored in remember/derived state of
                if(isNonEmptyPreviewOne) {
                    PreviewItem(
                        itemToPreview = noteWithPreview.previewOne,
                        noteFormat = noteWithPreview.note.noteFormat ?: TO_DO_LIST_NOTE_FORMAT,
                        openNoteOrToDo = { onClick(noteWithPreview.note) }
                    )
                    Spacer(modifier = Modifier.height(2.dp))
                }
                //preview 2
                if(isNonEmptyPreviewTwo) {
                    PreviewItem(
                        itemToPreview = noteWithPreview.previewTwo,
                        noteFormat = noteWithPreview.note.noteFormat ?: TO_DO_LIST_NOTE_FORMAT,
                        openNoteOrToDo = { onClick(noteWithPreview.note) }
                    )
                    Spacer(modifier = Modifier.height(2.dp))
                }
                //preview 3
                if(isNonEmptyPreviewThree)
                    PreviewItem(
                        itemToPreview = noteWithPreview.previewThree,
                        noteFormat = noteWithPreview.note.noteFormat ?: TO_DO_LIST_NOTE_FORMAT,
                        openNoteOrToDo = { onClick(noteWithPreview.note) }
                    )
            }
            //label
            if(isNonEmptyLabelName){
                Row(modifier = modifier
                    .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End)
                {
                    Text(
                        text = noteWithPreview.labelName,
                        fontSize = 10.sp,
                        color = InactiveTextColorLightHue,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier
                            .padding(0.dp)
                            .border(
                                width = 1.dp,
                                color = InactiveTextColorLightHue,
                                shape = RoundedCornerShape(6.dp)
                            )
                            .padding(5.dp),
                        maxLines = 1,
                        // TODO: this too can use a remember block
                        overflow = if(noteWithPreview.labelName.length > 20) TextOverflow.Ellipsis else TextOverflow.Visible
                    )
                }
            }
            //date time
            if(isNonNullLastUpdated){
                Text(text = DateTimeHelper.getLastUpdatedDate(noteWithPreview.note.lastUpdated),
                    modifier = modifier
                        .fillMaxWidth()
                        .padding(top = 3.dp),
                    textAlign = TextAlign.Center, fontSize = 10.sp, color = InactiveTextColor, fontWeight = FontWeight.SemiBold)
            }
        }
    }
}