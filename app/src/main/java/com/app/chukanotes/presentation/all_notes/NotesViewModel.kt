package com.app.chukanotes.presentation.all_notes

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.chukanotes.data.local.entities.NoteEntity
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.NoteWithPreview
import com.app.chukanotes.domain.repositories.NoteAndToDoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import javax.inject.Inject

@HiltViewModel
class NotesViewModel @Inject constructor(
    private val repository: NoteAndToDoRepository,
) : ViewModel() {

    private var _notes = mutableStateListOf<NoteWithPreview>()
    val notes : List<NoteWithPreview>
        get() = _notes.sortedWith(
            compareByDescending<NoteWithPreview> { it.note.rank }.thenByDescending { it.note.lastUpdated }
        ).toList()
    var isLoading by mutableStateOf(false)

    init {
      getNotes()
    }

    private var _notesQueuedForDelete = mutableStateListOf<NoteWithPreview>()
    val notesQueuedForDelete: List<NoteWithPreview>
        get() = _notesQueuedForDelete

    private var _todosQueuedForDelete = mutableStateListOf<ToDoItem>()
    private val todosQueuedForDelete: List<ToDoItem>
        get() = _todosQueuedForDelete

    private var _lastDeletedNotes = mutableSetOf<NoteWithPreview>()
    private var _lastDeletedToDos = mutableSetOf<ToDoItem>()

    var searchTextFieldValue by mutableStateOf("")

    private val _uiEvent = Channel<UiEvent>()
    val uiEvent = _uiEvent.receiveAsFlow()

    var notesToolbarState by mutableStateOf(NotesToolbarState.Default)
        private set

    private var job: Job? = null

    private fun getNotes(searchQuery: String = "") = viewModelScope.launch {
        timber.log.Timber.d("Chuka-note: getNotes() called")
        isLoading = true
        repository.getFilteredNotesAll(searchQuery).collect {
            _notes.clear()
            _notes += it
            isLoading = false
        }
    }

    fun onEvent(event: NotesEvent) {
        when(event) {
            is NotesEvent.OnNoteLongPressed -> handleSelectedNoteEvent(event.noteWithPreview)
            is NotesEvent.ResetNotesQueuedForDelete -> viewModelScope.launch { resetNotesQueuedForDeleteCache() }
            is NotesEvent.OnNavigate -> {
                job?.cancel()
                job = viewModelScope.launch {
                    isLoading = true
                    _uiEvent.send(UiEvent.OnNavigate(event.noteId, event.route))
                    //isLoading = false //removed this so the loading spinner can show for small item size
                }
            }
            is NotesEvent.OnDeleteNotes -> {
                job?.cancel()
                job = viewModelScope.launch {
                    deleteQueuedNotesAndTodos()
                }
            }
            is NotesEvent.OnUndoDeleteNotes -> {
                job?.cancel()
                job = viewModelScope.launch {
                    val notesToRestoreCount = undoDelete()
                    _uiEvent.send(UiEvent.UndoDelete(notesToRestoreCount))
                }
            }
            is NotesEvent.OnSearchFieldChanged -> {
                searchTextFieldValue = event.newValue
            }
            is NotesEvent.OnShowSearchField -> {
                // toggle state
                notesToolbarState = if(notesToolbarState == NotesToolbarState.Default)
                    NotesToolbarState.Search else NotesToolbarState.Default
                //use an event to show keyboard
                viewModelScope.launch {
                    _uiEvent.send(UiEvent.ShowSoftKeyboard)
                }
            }
            is NotesEvent.OnNoteSearchTriggered -> { //trigger filtered search
                job?.cancel()
                job = viewModelScope.launch {
                    getNotes(searchTextFieldValue)
                    //hide soft keyboard
                    _uiEvent.send(UiEvent.HideSoftKeyboard)
                }
            }
            is NotesEvent.ResetDeletedItemsCache -> viewModelScope.launch {
                _notesQueuedForDelete.clear()
                _todosQueuedForDelete.clear()
                _lastDeletedNotes.clear()
                _lastDeletedToDos.clear()
                notesToolbarState = NotesToolbarState.Default
            }
            is NotesEvent.OnCreateNoteWithToDoItem -> viewModelScope.launch {
                createNoteWithToDoItem(format = event.format, route = event.route)
            }
        }
    }

    private fun createNoteWithToDoItem(
        format: String,
        route: String
    ) = viewModelScope.launch {
        //set rank
        val rankValue = repository.getHighestRankForUnPinnedState() ?: 0
        //create note object
        val note = NoteEntity().apply {
            title = ""
            rank = rankValue
            noteFormat = format
            lastUpdated = OffsetDateTime.now()
        }
        //create default todoItem
        val toDoItem = ToDoItem()
        val newNoteId = repository.upsertNoteWithSingleTodo(note.toNote(), toDoItem)
        _uiEvent.send(UiEvent.OnNavigate(newNoteId, route))
    }

    private fun resetNotesQueuedForDeleteCache() = viewModelScope.launch{
        _notesQueuedForDelete.clear()
        _todosQueuedForDelete.clear()
        _lastDeletedNotes.clear()
        _lastDeletedToDos.clear()
        searchTextFieldValue = ""
        if(notesToolbarState == NotesToolbarState.Search){
            getNotes()
        }
        //reset search state
        notesToolbarState = NotesToolbarState.Default
    }

    private fun handleSelectedNoteEvent(noteWithPreview: NoteWithPreview) {
        if(noteWithPreview !in notesQueuedForDelete){ //means we enqueue for delete
            job?.cancel()
            job = viewModelScope.launch {
                //set state to delete state here
                notesToolbarState = NotesToolbarState.DeleteNote
                _notesQueuedForDelete.add(noteWithPreview)
                val resultTodos = repository.getAllItemsFromToDoWith(noteWithPreview.note.id!!)
                _todosQueuedForDelete.addAll(resultTodos)
            }
        }else{
            //remove note and it's corresponding to-dos from the queue for deletion
            _notesQueuedForDelete.remove(noteWithPreview)
            _todosQueuedForDelete.removeAll{ it.noteId == noteWithPreview.note.id }
            //set notes state based on if queue is empty
            if(_notesQueuedForDelete.isEmpty()) // means queue is empty after this un-select action
                notesToolbarState = NotesToolbarState.Default
        }
    }

    private suspend fun deleteQueuedNotesAndTodos() {
        // removed isLoading as this was causing re-composition.
        //isLoading = true
        //set notes state to default
        notesToolbarState = NotesToolbarState.Default
        //cache items about to be deleted
        _lastDeletedToDos.addAll(todosQueuedForDelete)
        //get notes size for snackBar message
        val notesToDeleteCount = _notesQueuedForDelete.size
        _lastDeletedNotes.addAll(_notesQueuedForDelete)
       // _notes -= _notesQueuedForDelete
        _notes.removeAll(_notesQueuedForDelete)
        repository.deleteNotesWithNoteObject(_notesQueuedForDelete.map { it.note.toNoteEntity() })
        _todosQueuedForDelete.clear()
        _notesQueuedForDelete.clear()
        //isLoading = false
        _uiEvent.send(UiEvent.OnDeleteNotes(notesToDeleteCount)) // this shows the snackBar
    }

    private suspend fun undoDelete(): Int {
        val notesToRestoreCount = _lastDeletedNotes.size
        //insert notes
        repository.upsertAllNotesWhereNotesIn(_lastDeletedNotes.map { it.note.toNoteEntity() })
        //insert toDoItems
        repository.upsertToDoItemsFor(_lastDeletedToDos.toList())
        _notes += _lastDeletedNotes
        //reset
        _lastDeletedToDos.clear()
        _lastDeletedNotes.clear()
        return notesToRestoreCount
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
        job = null
    }
}

sealed class UiEvent{
    data class OnNavigate(val noteId: Long, val route: String): UiEvent()
    data class OnDeleteNotes(val notesToDeleteCount: Int): UiEvent()
    data class UndoDelete(val notesToRestoreCount: Int): UiEvent()
    object HideSoftKeyboard: UiEvent()
    object ShowSoftKeyboard: UiEvent()
}

enum class NotesToolbarState{
    Default,
    Search,
    DeleteNote
}