package com.app.chukanotes.presentation.util

object Screens {
    const val ALL_NOTES = "all_notes"
    const val TODO_ITEM = "todo_item"
    const val PLAIN_NOTE = "plain_note"
}