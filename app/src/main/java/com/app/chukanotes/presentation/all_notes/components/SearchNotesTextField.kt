package com.app.chukanotes.presentation.all_notes.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.CheckboxColor
import com.app.chukanotes.ui.theme.InactiveTextColor

@ExperimentalComposeUiApi
@Composable
fun SearchNotesTextField(
    modifier: Modifier = Modifier,
    searchTextFieldValue: String,
    hintText: String,
    showKeyBoard: Boolean,
    onSearchFieldChanged: (String) -> Unit,
    onNoteSearchTriggered: () -> Unit
){
    val focusRequester = remember { FocusRequester() }
    LaunchedEffect(key1 = focusRequester){
        if(showKeyBoard)
            focusRequester.requestFocus()
    }

    Box(modifier = modifier.fillMaxSize()){
        Box(modifier = Modifier
            .fillMaxWidth()
            .height(1.dp)
            .background(
                AlmostWhiteBackground
            )
            .align(Alignment.BottomCenter)
            .padding(5.dp))
        BasicTextField(
            value = searchTextFieldValue,
            onValueChange = {newValue -> onSearchFieldChanged(newValue) },
            modifier = modifier.fillMaxWidth().focusRequester(focusRequester),
            singleLine = true,
            textStyle = TextStyle(
                fontSize = 18.sp, color = AlmostWhiteBackground,
            ),
            keyboardOptions = KeyboardOptions( imeAction = ImeAction.Search),
            keyboardActions = KeyboardActions( onSearch = {
                onNoteSearchTriggered()
            }),
            cursorBrush = SolidColor(CheckboxColor)
        )
        if(searchTextFieldValue.isEmpty()) {
            Text(
                text = hintText,
                fontSize = 18.sp,
                fontWeight = FontWeight.Normal,
                color = InactiveTextColor,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}