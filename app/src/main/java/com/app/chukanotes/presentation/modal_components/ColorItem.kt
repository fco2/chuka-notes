package com.app.chukanotes.presentation.modal_components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground

@Composable
fun ColorItem(
    colorName: String,
    color: Long,
    currentNoteColor: Long,
    onColorSelected: (Long) -> Unit
){
    val borderModifierActive = BorderStroke(width = 1.dp, color =  AlmostWhiteBackground)
    val borderStrokeInactive = BorderStroke(width = 0.dp, color = Color.Transparent)
    val interactionSource = remember { MutableInteractionSource() }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp)
            .border(if (currentNoteColor == color) borderModifierActive else borderStrokeInactive)
            .padding(start = 10.dp)
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) { onColorSelected(color) },
        verticalAlignment = Alignment.CenterVertically,
       horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(text = colorName, fontSize = 14.sp, fontWeight = FontWeight.Bold, color = AlmostWhiteBackground,
        modifier = Modifier.weight(0.3f))

        Box(modifier = Modifier
            .weight(0.7f)
            .height(40.dp)
            .padding(vertical = 8.dp, horizontal = 12.dp)
            .background(
                color = Color(color),
                shape = RoundedCornerShape(8.dp)
            ))
        if(currentNoteColor == color)
            Box(modifier = Modifier
                .padding(10.dp)
                .size(10.dp)
                .background(color = Color.Green, shape = CircleShape))
    }
}