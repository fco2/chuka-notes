package com.app.chukanotes.presentation.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.net.toUri
import com.app.chukanotes.R
import com.app.chukanotes.core.Constants.BASE_URI
import com.app.chukanotes.presentation.MainActivity
import timber.log.Timber


@ExperimentalAnimationApi
@ExperimentalStdlibApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@ExperimentalComposeUiApi
class CustomAlarm : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        try {
            val bundle = intent.extras
            val contentText = bundle!!.getString("alarm_message")!!
            val toDoIdToInt = bundle.getLong("toDoItemId").toInt()
            val noteId = bundle.getLong("noteId")
            showNotification(context, contentText, noteId, toDoIdToInt)
        } catch (e: Exception) {
            Timber.d("Chuka-note Error!! ${e.localizedMessage}")
        }
    }

    private val channelId = "chuka_notes_todos"

    @ExperimentalComposeUiApi
    @ExperimentalFoundationApi
    @ExperimentalStdlibApi
    @ExperimentalMaterialApi
    fun showNotification(
        context: Context,
        contentText: String,
        noteId: Long,
        toDoIdToInt: Int
    ) {

        val notificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val description = "Creates notification for toDo items with a date/time set."

        val channel =  NotificationChannel(
                channelId,
                "Notify ToDo Items Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
        // notificationManager.getNotificationChannel(channelId)
        //            ?:

        channel.description = description
        notificationManager.createNotificationChannel(channel)

        val builder = NotificationCompat.Builder(context, channelId).apply {
            setContentTitle("ToDo Item")
            setContentText(contentText)
            setSmallIcon(R.drawable.to_do_notification_icon)
            setAutoCancel(true)
            priority = NotificationCompat.PRIORITY_DEFAULT
            setContentIntent(getPendingIntent(context, noteId, toDoIdToInt))
        }
        //Timber.d("Chuka-note got to showNotification")
        with(NotificationManagerCompat.from(context)){
            notify(toDoIdToInt, builder.build())
        }
    }

    @ExperimentalComposeUiApi
    @ExperimentalFoundationApi
    @ExperimentalStdlibApi
    @ExperimentalMaterialApi
    private fun getPendingIntent(context: Context, noteId: Long, toDoIdToInt: Int): PendingIntent {
        val uri = "$BASE_URI/${Screens.TODO_ITEM}?noteId=${noteId}".toUri()
        val taskDetailIntent =Intent(Intent.ACTION_VIEW, uri, context, MainActivity::class.java)
        //val taskDetailIntent = Intent(context, MainActivity::class.java)
        val pendingIntent = TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(taskDetailIntent)
            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.S)
                getPendingIntent(toDoIdToInt, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE)
            else
                getPendingIntent(toDoIdToInt, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
        }
        return pendingIntent
    }
}