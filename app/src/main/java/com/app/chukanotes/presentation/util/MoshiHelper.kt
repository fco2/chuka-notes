package com.app.chukanotes.presentation.util

import com.app.chukanotes.domain.models.Note
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

object MoshiHelper {
    fun convertNoteToJsonString(note: Note, moshi: Moshi): String {
        val jsonAdapter = moshi.adapter(Note::class.java)
        return jsonAdapter.toJson(note)
    }
    //jsonString: {"title":"new note title","label":"","color":"#263238","rank":3,"noteFormat":"ToDoList","id":11,"lastUpdated":"2022-05-10T07:48:02.346-04:00"}
    //can't allow # in title string.
    //D/ToDoListViewModel: CHUKA-note...jsonString: {"title":"new note title","label":"","color":"\\
    //D/ToDoListViewModel: CHUKA-note exception...Unterminated string at path $.color

    fun convertJsonStringToNote(jsonString: String, moshi: Moshi): Note {
        val jsonAdapter: JsonAdapter<Note> = moshi.adapter(Note::class.java).lenient()
        return jsonAdapter.fromJson(jsonString)!!
    }
}