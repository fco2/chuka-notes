package com.app.chukanotes.presentation.util

import android.content.Context
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.SnackbarResult
import com.app.chukanotes.R
import com.app.chukanotes.presentation.util.CoroutineJob.job
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object SnackBarUtil {
    fun showSnackBarByType(
        type: SnackbarMessageType,
        snackBarHostState: SnackbarHostState,
        coroutineScope: CoroutineScope,
        context: Context,
        undoDeleteEvent: (() -> Unit)? = null,
        resetDeletedItemsCache: (() -> Unit)? = null,
        hasMultipleItems: Boolean = false
    ){
        job?.cancel() // this cancels the last job and starts a new one
        snackBarHostState.currentSnackbarData?.dismiss()
        job = coroutineScope.launch {
            if(type == SnackbarMessageType.Deleted){
                val deletedMessage = if(hasMultipleItems)
                    context.getString(R.string.deleted_multiple_items_message)
                else context.getString(R.string.deleted_item_message)
                val result = snackBarHostState.showSnackbar(deletedMessage, actionLabel = "Undo")
                if(result == SnackbarResult.ActionPerformed){
                    undoDeleteEvent?.invoke()
                }
                if(result == SnackbarResult.Dismissed){ // clear
                    resetDeletedItemsCache?.invoke()
                }
            }else{
                val restoredMessage = if(hasMultipleItems)
                    context.getString(R.string.successfully_restored_multiple_items)
                else context.getString(R.string.successfully_restored_item)
                val result = snackBarHostState.showSnackbar(restoredMessage)
                if(result == SnackbarResult.Dismissed){ // clear
                    resetDeletedItemsCache?.invoke()
                }
            }
        }
    }
}

object CoroutineJob{
    var job: Job? = null
}