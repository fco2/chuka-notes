package com.app.chukanotes.presentation.plain_note

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import com.app.chukanotes.presentation.todo_list.components.BasicTitleTextField
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.InactiveTextColor
import kotlinx.coroutines.launch

@Composable
fun PlainNoteItem(
    modifier: Modifier = Modifier,
    cutCornerSize: Dp = 40.dp,
    noteColor: Color,
    title: String,
    content: String,
    titleHintText: String,
    contentHintText: String,
    cursorBrush: Brush = SolidColor(Color.Green),
    focusRequester: FocusRequester,
    onTitleTextChanged: (String) -> Unit,
    onContentTextChanged: (String) -> Unit
) {
    val cornerRadius = 12.dp
    val noteBorderScale = remember{ Animatable(initialValue = 1f) }

    LaunchedEffect(key1 = noteBorderScale){
        launch {
            noteBorderScale.animateTo(0.9f, animationSpec = tween(durationMillis = 50))
            noteBorderScale.animateTo(targetValue = 1f,
                animationSpec = spring(dampingRatio = Spring.DampingRatioLowBouncy, stiffness = Spring.StiffnessLow))
        }
    }

    LaunchedEffect(Unit){
        if(content.isEmpty())
            focusRequester.requestFocus()
    }

    Box(modifier = modifier.scale(noteBorderScale.value)) {
        Canvas(modifier = modifier.matchParentSize()) {
            val clipPath = Path().apply {
                lineTo(size.width - cutCornerSize.toPx(), 0f)
                lineTo(size.width, cutCornerSize.toPx())
                lineTo(size.width, size.height)
                lineTo(0f, size.height)
                close()
            }

            clipPath(clipPath) {
                drawRoundRect(
                    color = noteColor,
                    size = size, // this will just take size of the canvas
                    cornerRadius = CornerRadius(cornerRadius.toPx()),
                )
                //cut corner
                drawRoundRect(
                    color = Color(
                        ColorUtils.blendARGB(noteColor.toArgb(), 0x000000, 0.3f)
                    ),
                    topLeft = Offset(
                        size.width - cutCornerSize.toPx(),
                        -100f
                    ), // -100f moves the smaller box up so we don't see rounding in the bigger box
                    size = Size(
                        cutCornerSize.toPx() + 100f,
                        cutCornerSize.toPx() + 100f
                    ), // this is the smaller box size. the 100 is to account for the offset
                    cornerRadius = CornerRadius(10.dp.toPx()),
                )
            }
        }
        //under the canvas, put content of your note here
        LazyColumn(
            modifier
                .fillMaxSize()
                .padding(start = 10.dp, end = cutCornerSize),
            verticalArrangement = Arrangement.Top
        )
        {
            item {
                //title box
                Box(
                    modifier = modifier
                        .fillMaxWidth()
                        .padding(top = 10.dp)
                ) {
                    //text field
                    BasicTitleTextField(
                        modifier = modifier
                            .fillMaxWidth()
                            .padding(4.dp)
                            .align(Alignment.TopStart),
                        noteTitle = title,
                        onTitleFieldValueChanged = { newValue -> onTitleTextChanged(newValue) }
                    )
                    //hint
                    if (title.isEmpty()) {
                        Text(
                            text = titleHintText,
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Bold,
                            color = InactiveTextColor,
                            modifier = modifier
                                .fillMaxWidth()
                                .padding(start = 6.dp)
                        )
                    }
                }
            }
            item {
                Spacer(modifier = Modifier.height(10.dp))
            }
            //Content box
            item {
                Box(
                    modifier = modifier
                        .fillMaxSize()
                ) {
                    BasicTextField(
                        value = content,
                        onValueChange = {
                            newValue ->
                            onContentTextChanged(newValue)
                        },
                        modifier = modifier
                            .fillMaxWidth()
                            .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 300.dp)
                            .focusRequester(focusRequester),
                        singleLine = false,
                        maxLines = Int.MAX_VALUE,
                        textStyle = TextStyle(
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Normal,
                            color = AlmostWhiteBackground,
                            textDecoration = TextDecoration.None
                        ),
                        cursorBrush = cursorBrush,
                        keyboardOptions = KeyboardOptions.Default.copy(
                            capitalization = KeyboardCapitalization.Sentences, autoCorrect = true
                        )
                    )
                    if (content.isEmpty()) {
                        Text(
                            text = contentHintText,
                            fontSize = 17.sp,
                            fontWeight = FontWeight.Bold,
                            color = InactiveTextColor,
                            modifier = modifier
                                .fillMaxWidth()
                                .padding(start = 6.dp)
                        )
                    }
                }
            }
            //to push list up a bit above keyboard
            item {
                Spacer(modifier = Modifier.height(100.dp))
            }
        }
    }
}