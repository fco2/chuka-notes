package com.app.chukanotes.presentation.all_notes

import com.app.chukanotes.domain.models.NoteWithPreview

sealed class NotesEvent{
    data class OnNoteLongPressed(val noteWithPreview: NoteWithPreview): NotesEvent()
    object ResetNotesQueuedForDelete: NotesEvent()
    data class OnNavigate(val noteId: Long, val route: String): NotesEvent()
    object OnDeleteNotes: NotesEvent()
    object OnUndoDeleteNotes: NotesEvent()
    data class OnSearchFieldChanged(val newValue: String): NotesEvent()
    object OnShowSearchField: NotesEvent()
    object OnNoteSearchTriggered: NotesEvent()
    object ResetDeletedItemsCache: NotesEvent()
    data class OnCreateNoteWithToDoItem(val format: String, val route: String): NotesEvent()
}