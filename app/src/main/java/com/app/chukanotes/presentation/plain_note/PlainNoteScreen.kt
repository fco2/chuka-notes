package com.app.chukanotes.presentation.plain_note

import android.content.Context
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.chukanotes.R
import com.app.chukanotes.presentation.modal_components.ModalType
import com.app.chukanotes.presentation.modal_components.SheetContent
import com.app.chukanotes.presentation.util.*
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.DarkerGray
import com.app.chukanotes.ui.theme.NotSoBlack
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
fun PlainNoteScreen(
    modifier: Modifier = Modifier,
    viewModel: PlainNoteViewModel = hiltViewModel(),
    navController: NavController
){
    val context = LocalContext.current
    var modalType by rememberSaveable { mutableStateOf(ModalType.None) }
    var modalTitle by rememberSaveable { mutableStateOf("") }
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val modalBottomSheetState = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
    animationSpec = tween(durationMillis = 300, easing = FastOutSlowInEasing)
        )
    var shouldConfirmDeleteNote by rememberSaveable{  mutableStateOf(false) }

    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current

    //do on backPressed action
    val onBackAction: () -> Unit = {
        //Timber.d("CHUKA-note: modalSheetState: ${modalBottomSheetState.isVisible}")
        if(!modalBottomSheetState.isVisible) {
            focusManager.clearFocus()
            handleBackAction(viewModel)
        }
        else
            coroutineScope.launch { modalBottomSheetState.hide() }
    }
    //instantiate back pressed object
    BackPressedHandler(onBackPressed = onBackAction)

    LaunchedEffect(key1 = true){
        viewModel.uiEvent.collectLatest { event ->
            when(event) {
                is UiEvent.OnBackPressed -> {
                    navController.navigate(Screens.ALL_NOTES){
                        launchSingleTop = true
                    }
                }
                is UiEvent.OnLabelModalOpened -> {
                    focusManager.clearFocus(true)
                    modalType = ModalType.Label
                    coroutineScope.launch { modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded) }
                }
                is UiEvent.OnNavigateToNotesScreen -> {
                    navController.navigate(Screens.ALL_NOTES){
                        launchSingleTop = true
                    }
                }
                else -> Unit
            }
        }
    }

    if(viewModel.state.isScreenContentInitialized){
        ModalBottomSheetLayout(
            sheetState = modalBottomSheetState,
            sheetContentColor = Color.Transparent,
            sheetBackgroundColor = Color.Transparent,
            sheetElevation = 8.dp,
            sheetContent = {
                //bottom sheet modal for colors and labels
                SheetContent(
                    modifier = modifier,
                    modalType = modalType,
                    modalTitle = modalTitle,
                    currentNoteColor = viewModel.state.currentNoteColor,
                    currentNoteLabel = viewModel.state.currentNoteLabel,
                    currentEditingLabelText = viewModel.state.currentEditingLabelText,
                    labelList = viewModel.labelList.toList(),
                    onColorSelected = { selectedColor ->
                        viewModel.onEvent(PlainNoteEvent.OnNoteColorChanged(viewModel.state.note, selectedColor))
                    },
                    onLabelCheckboxChanged = { labelText, checked ->
                        viewModel.onEvent(PlainNoteEvent.OnLabelCheckboxChecked(labelText, checked)) },
                    onLabelTextFieldChanged = { newValue ->  viewModel.onEvent(PlainNoteEvent.OnLabelTextFieldChanged(newValue))},
                    onDeleteLabel = {labelTitle -> viewModel.onEvent(PlainNoteEvent.OnDeleteLabel(labelTitle)) },
                    onSaveNewLabel = { viewModel.onEvent(PlainNoteEvent.OnSaveNewLabel) },
                    titleHintText =  context.getString(R.string.add_label)
                )
            },
            modifier = Modifier.fillMaxWidth(),
            sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
        ) {
            PlainNoteScreenContent(
                viewModel = viewModel,
                context = context,
                modifier = modifier,
                shouldConfirmDeleteNote = shouldConfirmDeleteNote,
                coroutineScope = coroutineScope,
                modalBottomSheetState = modalBottomSheetState,
                focusRequester = focusRequester,
                onSetModalTypeAndTitle = { type, title ->
                    modalType = type
                    modalTitle = title
                },
                onSetShouldConfirmDeleteNote = { flag ->  shouldConfirmDeleteNote = flag},
                onConfirmDeleteNote = { newValue -> shouldConfirmDeleteNote = newValue }
            )
        }
    }
}

private fun handleBackAction(viewModel: PlainNoteViewModel){
    viewModel.onEvent(PlainNoteEvent.OnBackPressed)
}

@ExperimentalMaterialApi
@Composable
fun PlainNoteScreenContent(
    modifier: Modifier = Modifier,
    viewModel: PlainNoteViewModel,
    context: Context,
    shouldConfirmDeleteNote: Boolean,
    coroutineScope: CoroutineScope,
    modalBottomSheetState: ModalBottomSheetState,
    focusRequester: FocusRequester,
    onSetModalTypeAndTitle: (ModalType, String) -> Unit,
    onSetShouldConfirmDeleteNote: (Boolean) -> Unit,
    onConfirmDeleteNote: (Boolean) -> Unit
){
    val interactionSource = remember { MutableInteractionSource() }
    val focusManager = LocalFocusManager.current

    Box(modifier = modifier.fillMaxSize()){
        Surface(elevation = 8.dp) {
            Box(modifier = Modifier
                .fillMaxSize()
                .background(NotSoBlack)
                .padding(top = 72.dp, start = 8.dp, end = 8.dp, bottom = 8.dp))
            {
                PlainNoteItem(
                    modifier = Modifier
                        .fillMaxSize(),
                    noteColor = Color(viewModel.state.currentNoteColor),
                    titleHintText = context.getString(R.string.enter_title),
                    title = viewModel.state.note.title,
                    content = viewModel.state.plainNoteContent,
                    focusRequester = focusRequester,
                    onTitleTextChanged = { newValue -> viewModel.onEvent(PlainNoteEvent.OnTitleTextChanged(newValue)) },
                    onContentTextChanged = { newValue -> viewModel.onEvent(PlainNoteEvent.OnContentTextChanged(newValue))},
                    contentHintText = context.getString(R.string.enter_note)
                )
            }
        }
        //This has to remain at the bottom so that the todos scroll directly under the toolbar
        Row(modifier = modifier
            .fillMaxWidth()
            .height(64.dp)
            .padding(0.dp)
            .background(DarkerGray)
            .padding(10.dp),
            verticalAlignment = Alignment.Top,
            horizontalArrangement = Arrangement.SpaceBetween)
        {
            //delete icon
            Image(
                painter = painterResource(id = R.drawable.trash_64),
                contentDescription = context.getString(R.string.choose_label),
                modifier = modifier
                    .padding(horizontal = 10.dp, vertical = 8.dp)
                    .size(40.dp)
                    .clickable(
                        interactionSource = interactionSource,
                        indication = null
                    ) { onSetShouldConfirmDeleteNote(true) }
            )
            Row(modifier = modifier
                .height(64.dp)
                .padding(0.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly)
            {
                Image(
                    painter = painterResource(id = if(viewModel.state.isPinned) R.drawable.pinned_icon
                    else R.drawable.unpinned_icon),
                    contentDescription = context.getString(R.string.pin_note_or_todo),
                    modifier = modifier
                        .padding(horizontal = 12.dp, vertical = 8.dp)
                        .clickable(
                            interactionSource = interactionSource,
                            indication = null
                        ) {
                            viewModel.onEvent(PlainNoteEvent.OnPinnedStateToggle)
                        }
                )
                Image(
                    painter = painterResource(id = R.drawable.tag),
                    contentDescription = context.getString(R.string.choose_label),
                    modifier = modifier
                        .clickable(
                            interactionSource = interactionSource,
                            indication = null
                        ) {
                            viewModel.onEvent(PlainNoteEvent.OnLabelModalOpened)
                        }
                        .padding(horizontal = 12.dp, vertical = 8.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.color_picker_124),
                    contentDescription = context.getString(R.string.choose_color),
                    modifier = modifier
                        .padding(horizontal = 12.dp, vertical = 8.dp)
                        .clickable(
                            interactionSource = interactionSource,
                            indication = null
                        ) {
                            //set modal type and title
                            onSetModalTypeAndTitle(
                                ModalType.Color,
                                context.getString(R.string.select_note_background_color)
                            )
                            focusManager.clearFocus(true)
                            coroutineScope.launch {
                                modalBottomSheetState.animateTo(
                                    ModalBottomSheetValue.Expanded
                                )
                            }
                        }
                )// Share note button
                val stringToShare = StringBuilder()
                //append title
                if (!viewModel.state.isTitleEmpty)
                    stringToShare.append("*${viewModel.state.titleText}*\n")
                // append content
                stringToShare.append(viewModel.state.plainNoteContent)
                // only share if there is content
                if (!viewModel.state.isTitleEmpty || viewModel.state.plainNoteContent.isNotBlank()) {
                    ShareItemAsPlainText(
                        context,
                        modifier,
                        interactionSource,
                        stringToShare,
                        context.getString(R.string.share_note_title)
                    )
                }
            }
        }
        //Confirm delete modal
        if (shouldConfirmDeleteNote) {
            focusManager.clearFocus(true)
            Box(modifier = Modifier
                .fillMaxSize()
                .background(Color.Black.copy(alpha = 0.5f))
                .clickable(enabled = false, onClick = {})){ // empty onClick to remove ripple effect in button
                ConfirmDeleteNoteModal(
                    modifier,
                    context,
                    shouldConfirmDeleteNote,
                    viewModel.state.currentNoteColor,
                    onDeleteNote = { viewModel.onEvent(PlainNoteEvent.OnDeleteNote) },
                    onConfirmDeleteNote = { newValue -> onConfirmDeleteNote(newValue) }
                )
            }
        }
        //Circular progress bar
        if(viewModel.state.isLoading){
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center), color = AlmostWhiteBackground)
        }
    }
}