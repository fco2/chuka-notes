package com.app.chukanotes.presentation.plain_note

import com.app.chukanotes.domain.models.Note

sealed class PlainNoteEvent{
    data class OnTitleTextChanged(val newTextValue: String): PlainNoteEvent()
    data class OnContentTextChanged(val newTextValue: String): PlainNoteEvent()
    object OnBackPressed: PlainNoteEvent()
    data class OnNoteColorChanged(val note: Note, val newColor: Long): PlainNoteEvent()
    data class OnLabelTextFieldChanged(val labelText: String): PlainNoteEvent()
    data class OnLabelCheckboxChecked(val labelText: String, val isChecked: Boolean): PlainNoteEvent()
    object OnLabelModalOpened: PlainNoteEvent()
    data class OnDeleteLabel(val labelTitle: String): PlainNoteEvent()
    object OnSaveNewLabel: PlainNoteEvent()
    object OnDeleteNote: PlainNoteEvent()
    object OnPinnedStateToggle: PlainNoteEvent()
}
