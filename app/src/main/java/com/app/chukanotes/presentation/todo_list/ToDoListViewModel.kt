package com.app.chukanotes.presentation.todo_list

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.chukanotes.core.Resource
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.Label
import com.app.chukanotes.domain.repositories.NoteAndToDoRepository
import com.app.chukanotes.presentation.util.NotificationCalendar
import com.app.chukanotes.presentation.util.SnackbarMessageType
import com.app.chukanotes.presentation.util.UiEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import java.util.*
import javax.inject.Inject
import kotlin.math.max

@HiltViewModel
class ToDoListViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val repository: NoteAndToDoRepository
): ViewModel() {

    var toDoListState by mutableStateOf(ToDoListState())
        private set

    private var _toDos = emptyList<ToDoItem>().toMutableStateList()
    val toDos: List<ToDoItem>
        get() = _toDos.sortedBy { it.id }

    private var _labelList = emptyList<String>().toMutableStateList()
    val labelList: Set<String>
        get() = _labelList.toMutableSet()

    private val _uiEvent = Channel<UiEvent>()
    val uiEvent = _uiEvent.receiveAsFlow()

    private var currentlyEditingTodo by mutableStateOf(ToDoItem())

    private var job: Job? = null

    init{
        loadData()
    }

    private fun loadData() = viewModelScope.launch {
        val id = savedStateHandle.get<Long>("noteId")!!
        val noteFromSavedState = repository.getSingleNote(id)?.toNote()

        //check if null noteFromSavedState, then navigate to home screen
        if(noteFromSavedState == null){
            _uiEvent.send(UiEvent.OnNavigateToNotesScreen)
        }else{
            toDoListState = toDoListState.copy(note = noteFromSavedState)

            val result = repository.getAllItemsFromToDoWith(toDoListState.note.id!!)
            toDoListState = toDoListState.copy(
                isLoading = false,
                titleText = noteFromSavedState.title,
                isTitleEmpty = noteFromSavedState.title.isBlank(),
                currentNoteColor = noteFromSavedState.color,
                isPinned = noteFromSavedState.isPinned
            )
            _toDos.addAll(result)
        }
    }

    fun onEvent(event: ToDoEvent) {
        when(event){
            is ToDoEvent.OnAddToDo -> {
                job?.cancel()
                job = viewModelScope.launch {
                    val blankToDoItem = ToDoItem().also { it.noteId = toDoListState.note.id!! }
                    val toDoId = repository.upsertToDoItem(blankToDoItem)
                    blankToDoItem.id = toDoId
                    _toDos.add(blankToDoItem)
                    //set last updated only when new to-do is added or title is changed
                    toDoListState.note.lastUpdated = OffsetDateTime.now()
                    //set here for alarm
                    with(currentlyEditingTodo){
                        if(message.contains("@")){
                            //set title, send uiEvent
                            viewModelScope.launch {
                                toDoListState.note.also { it.title = toDoListState.titleText }
                                val calendarObject: Calendar? = NotificationCalendar.getDateTimeToNotify(message)
                                if(calendarObject != null)
                                    _uiEvent.send(UiEvent.SetToDoAlarm(
                                        toDoListState.note.id!!,
                                        message,
                                        id!!,
                                        calendarObject.timeInMillis
                                    ))
                            }
                        }
                    }
                    currentlyEditingTodo = blankToDoItem
                }
            }
            is ToDoEvent.OnDeleteToDo -> {
                job?.cancel()
                job = viewModelScope.launch {
                    toDoListState = toDoListState.copy(lastDeletedToDo = event.toDoItem)
                    _toDos.remove(event.toDoItem)
                    repository.deleteToDoItemFromNote(toDoListState.note.id!!, event.toDoItem.id!!)
                    //send ui event to show snackbar to enable undo delete
                    val nullableToDoId = if(event.toDoItem.message.contains("@")) event.toDoItem.id else null
                    _uiEvent.send(UiEvent.ShowSnackbar(SnackbarMessageType.Deleted, nullableToDoId))
                }
            }
            is ToDoEvent.OnCheckedChanged -> {
                job?.cancel()
                job = viewModelScope.launch {
                    _toDos.remove(event.toDoItem)
                    event.toDoItem.isCompleted = event.checked
                    _toDos.add(event.toDoItem)
                    //TODO: fix
                    repository.upsertNoteWithTodos(
                        toDoListState.note.also {
                            it.title = toDoListState.titleText
                            it.color = toDoListState.currentNoteColor
                            //this will update timestamp for when to-do item is changed.
                            it.lastUpdated = OffsetDateTime.now()
                        },
                        toDoItems = toDos
                    )
                    //cancel alarm if exists
                    if(event.toDoItem.message.contains("@"))
                        _uiEvent.send(UiEvent.CancelAlarm(event.toDoItem.id!!))
                }
            }
            is ToDoEvent.OnTextFieldValueChanged -> {
               val listToUpdate = _toDos.toMutableList()
                listToUpdate.find { it.id == event.toDoItem.id }?.message = event.newValue
                _toDos.clear() // apparently, you have to clear list and re-add items in mutableStateList
                _toDos = listToUpdate.toMutableStateList()
                event.toDoItem.message = event.newValue
                //also set currently editing to-do item text
                currentlyEditingTodo = event.toDoItem
                //save note and to-dos
                job?.cancel()
                job = viewModelScope.launch {
                    delay(500L)
                    repository.upsertNoteWithTodos(
                        toDoListState.note.also {
                            it.title = toDoListState.titleText
                            it.color = toDoListState.currentNoteColor
                            //this will update timestamp for when to-do item is changed.
                            it.lastUpdated = OffsetDateTime.now()
                        },
                        toDoItems = toDos
                    )
                }
            }
            is ToDoEvent.OnBackPressed -> {
                job?.cancel()
                job = viewModelScope.launch {
                    toDoListState = toDoListState.copy(isLoading = true)
                    //only save note and to-do if both are not empty
                    if(isNoteAndTodosEmpty()){
                        //deleting note will cascade the todos
                        repository.deleteNote(toDoListState.note.toNoteEntity())
                    }else{
                        //didn't update lastUpdated as that will be handled when note contents actually change
                        repository.upsertNoteWithTodos(
                            toDoListState.note.also {
                                it.title = toDoListState.titleText
                                it.color = toDoListState.currentNoteColor
                                //this will update timestamp for when to-do item is changed.
                                if(currentlyEditingTodo.message != "") it.lastUpdated = OffsetDateTime.now()},
                            toDoItems = toDos
                        )
                    }
                    //this ensures that if a reminder is set for the last item, it will be captured.
                    if(currentlyEditingTodo.message.contains("@")){
                        toDoListState.note.also { it.title = toDoListState.titleText }
                        val calendarObject: Calendar? = NotificationCalendar.getDateTimeToNotify(currentlyEditingTodo.message)
                        calendarObject?.let {
                            _uiEvent.send(
                                UiEvent.OnBackPressed(
                                    toDoListState.note.id!!,
                                    currentlyEditingTodo.message,
                                    currentlyEditingTodo.id!!,
                                    it.timeInMillis
                                ))
                        }
                    }
                    _uiEvent.send(UiEvent.OnBackPressed())
                    //commenting this out to enable the loading spinner show, state does not even matter here
                    //toDoListState = toDoListState.copy(isLoading = false)
                }
            }
            is ToDoEvent.OnTitleFieldValueChanged -> {
                val updatedNote = toDoListState.note.also { it.lastUpdated = OffsetDateTime.now(); it.title = event.newValue}
                toDoListState = toDoListState.copy(note = updatedNote, isTitleEmpty = updatedNote.title.isBlank(),
                titleText = event.newValue)
                job?.cancel()
                job = viewModelScope.launch {
                    delay(2000L)
                    repository.upsertNoteWithTodos(
                        toDoListState.note,
                        toDoItems = toDos
                    )
                }
            }
            ToDoEvent.UnDoDelete -> {
                job?.cancel()
                job = viewModelScope.launch {
                    repository.upsertToDoItem(toDoListState.lastDeletedToDo!!)
                    _toDos.add(toDoListState.lastDeletedToDo!!)
                    toDoListState = toDoListState.copy(lastDeletedToDo = null) // reset last deleted here
                    _uiEvent.send(UiEvent.UnDoDelete)
                }
            }
            is ToDoEvent.OnNoteColorChanged -> {
                toDoListState = toDoListState.copy(
                    note = event.note.also { it.color = event.newColor },
                    currentNoteColor = event.newColor
                )
                job?.cancel()
                job = viewModelScope.launch {
                    repository.upsertNoteWithTodos(
                        toDoListState.note.also { it.lastUpdated = OffsetDateTime.now() },
                        toDoItems = toDos
                    )
                }
            }
            is ToDoEvent.OnLabelTextFieldChanged -> toDoListState = toDoListState.copy(currentEditingLabelText = event.labelText)
            is ToDoEvent.OnLabelCheckboxChecked -> viewModelScope.launch {
                toDoListState = if(event.isChecked){
                    val newLabel = Label(title = event.labelText, noteId = toDoListState.note.id!!)
                    repository.addLabel(newLabel)
                    toDoListState.copy(currentNoteLabel = event.labelText)
                }else{
                    repository.deleteLabel(toDoListState.currentNoteLabel) //first delete
                    val label = Label(title = toDoListState.currentNoteLabel, noteId = -1L)
                    repository.addLabel(label)
                    toDoListState.copy(currentNoteLabel = "")
                }
            }
            ToDoEvent.OnLabelModalOpened -> viewModelScope.launch {
                var gotToCollectBlock = false //used this flag as label list can be empty when first launched
                repository.getAllLabels().onEach { result ->
                    when(result){
                        is Resource.Success -> {
                            gotToCollectBlock = true
                            _labelList.addAll(result.data!!.map { it.title }.distinct())
                            //set currentNoteLabel here
                            result.data.forEach { label ->
                                if(label.noteId == toDoListState.note.id){
                                    toDoListState = toDoListState.copy(currentNoteLabel = label.title)
                                    return@forEach
                                }
                            }
                            toDoListState = toDoListState.copy(isLoading = false)
                            _uiEvent.send(UiEvent.OnLabelModalOpened)
                        }
                        is Resource.Loading -> toDoListState = toDoListState.copy(isLoading = true)
                        is Resource.Error -> Unit
                    }
                }.launchIn(viewModelScope)
                if(!gotToCollectBlock) _uiEvent.send(UiEvent.OnLabelModalOpened)
            }
            is ToDoEvent.OnDeleteLabel -> viewModelScope.launch {
                toDoListState = toDoListState.copy(isLoading = true)
                repository.deleteLabel(event.labelTitle)
                val tempList = mutableSetOf<String>()
                tempList.addAll(labelList)
                tempList.remove(event.labelTitle)
                _labelList.clear()
                _labelList = tempList.toMutableStateList()
                if(toDoListState.currentNoteLabel == event.labelTitle)
                    toDoListState = toDoListState.copy(currentNoteLabel = "") //clean up references
                toDoListState = toDoListState.copy(isLoading = false)
            }
            ToDoEvent.OnSaveNewLabel -> viewModelScope.launch {
                val newLabel = Label(title = toDoListState.currentEditingLabelText, noteId = toDoListState.note.id!!)
                repository.addLabel(newLabel)
                _labelList += toDoListState.currentEditingLabelText
                //set current note label before erasing it
                toDoListState = toDoListState.copy(currentNoteLabel = toDoListState.currentEditingLabelText)
                toDoListState = toDoListState.copy(currentEditingLabelText = "")
            }
            ToDoEvent.OnDeleteNote -> {
                job?.cancel()
                job = viewModelScope.launch{
                    toDoListState = toDoListState.copy(isLoading = true)
                    repository.deleteNote(toDoListState.note.toNoteEntity())
                    _uiEvent.send(UiEvent.OnNavigateToNotesScreen)
                    //toDoListState = toDoListState.copy(isLoading = false)
                }
            }
            ToDoEvent.OnPinnedStateToggle -> {
                job?.cancel()
                job = viewModelScope.launch {
                    //save existing isPinned state
                    val isCurrentlyPinned = toDoListState.isPinned
                    //check pinned state and update db accordingly
                    repository.getAllRankWithPinnedState().onEach { result ->
                        when(result){
                            is Resource.Success -> {
                                val sortedData = result.data!!.toMutableList()
                                sortedData.sortWith(compareByDescending { it.rank })
                                var newRank = -1L
                                //create updated Note object
                                val updatedNote = toDoListState.note.apply{
                                    title = toDoListState.titleText
                                    color = toDoListState.currentNoteColor
                                    lastUpdated = OffsetDateTime.now()
                                }
                                if(isCurrentlyPinned){ //then unpin note
                                    run runBlock@ {
                                        sortedData.forEach rankForLoop@{ rankWithPinnedState ->
                                            if(!rankWithPinnedState.isPinned){ //get first unPinned rank
                                                newRank = rankWithPinnedState.rank - 1L
                                                return@runBlock
                                            }
                                        }
                                    }
                                    updatedNote.apply{
                                        rank = if(newRank == -1L) 0 else newRank
                                        isPinned = false
                                    }
                                }else { //pin note
                                    //set rank to the highest
                                    sortedData.forEach{
                                        newRank = max(it.rank, newRank)
                                    }
                                    newRank++ //increment it so this ensures that note stays at top
                                    updatedNote.apply{
                                        rank = newRank
                                        isPinned = true
                                    }
                                }
                                repository.upsertNoteWithTodos(
                                    updatedNote.also { it.lastUpdated = OffsetDateTime.now() },
                                    toDoItems = toDos
                                )
                                toDoListState = toDoListState.copy(
                                    note = updatedNote,
                                    isPinned = !isCurrentlyPinned,
                                    isLoading = false
                                )
                            }
                            is Resource.Loading -> {
                                toDoListState = toDoListState.copy(isLoading = true)
                            }
                            else -> Unit
                        }
                    }.launchIn(viewModelScope)
                }
            }
        }
    }

    private fun isNoteAndTodosEmpty(): Boolean{
        var isAllTodosEmpty = true
        toDos.forEach forLoop@{ toDo ->
            if(toDo.message.trim() != ""){
                isAllTodosEmpty = false
                return@forLoop
            }
        }
        return toDoListState.note.title.isEmpty() && isAllTodosEmpty
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}