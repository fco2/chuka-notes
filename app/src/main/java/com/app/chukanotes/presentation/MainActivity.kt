package com.app.chukanotes.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navDeepLink
import com.app.chukanotes.R
import com.app.chukanotes.core.Constants
import com.app.chukanotes.presentation.all_notes.NotesScreen
import com.app.chukanotes.presentation.plain_note.PlainNoteScreen
import com.app.chukanotes.presentation.todo_list.ToDoListScreen
import com.app.chukanotes.presentation.util.Screens
import com.app.chukanotes.ui.theme.ChukaNotesTheme
import com.app.chukanotes.ui.theme.NotSoBlack
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalComposeUiApi
@ExperimentalStdlibApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ChukaNotesTheme {
                Surface(modifier = Modifier
                    .fillMaxSize()
                    .background(NotSoBlack)) {
                    val notesTitle = getString(R.string.notes)
                    val navController = rememberNavController()

                    NavHost(navController = navController, startDestination =  Screens.ALL_NOTES){
                        composable(Screens.ALL_NOTES){
                            NotesScreen(navController = navController, notesTitle = notesTitle)
                        }
                        composable(route = Screens.TODO_ITEM + "?noteId={noteId}",
                            arguments = listOf(
                                navArgument(name = "noteId"){
                                    type = NavType.LongType
                                    defaultValue = -1L
                                }
                            ),
                            deepLinks = listOf(
                                navDeepLink { uriPattern = "${Constants.BASE_URI}/${Screens.TODO_ITEM}?noteId={noteId}" }
                            ),
                        )
                        {
                            ToDoListScreen(navController = navController)
                        }
                        composable(route = Screens.PLAIN_NOTE + "?noteId={noteId}",
                            arguments = listOf(
                                navArgument(name = "noteId"){
                                    type = NavType.LongType
                                    defaultValue = -1L
                                }
                            )){
                            PlainNoteScreen(navController = navController)
                        }
                    }
                }
            }
        }
    }
}