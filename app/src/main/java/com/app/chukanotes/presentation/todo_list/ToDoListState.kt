package com.app.chukanotes.presentation.todo_list

import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.Note

data class ToDoListState(
    val note: Note = Note(),
    val isTitleEmpty: Boolean = false,  //TODO: don't need this
    val titleText: String = "",
    val isLoading: Boolean = false,
    val lastDeletedToDo: ToDoItem? = null,
    val currentNoteColor: Long = 0xff263238,
    val currentNoteLabel: String = "",
    val currentEditingLabelText: String = "",
    val isPinned: Boolean = false,
)
