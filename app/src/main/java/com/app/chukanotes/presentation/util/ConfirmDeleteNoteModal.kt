package com.app.chukanotes.presentation.util

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.unit.dp
import androidx.core.graphics.ColorUtils
import com.app.chukanotes.R
import com.app.chukanotes.ui.theme.*

@Composable
fun ConfirmDeleteNoteModal(
    modifier: Modifier,
    context: Context,
    shouldConfirmDeleteNote: Boolean,
    noteColor: Long,
    onDeleteNote: () -> Unit,
    onConfirmDeleteNote: (Boolean) -> Unit
) {
    val interactionSource = remember { MutableInteractionSource() }

    Box(modifier = modifier.fillMaxSize().background(
        color = Color.Transparent,
        shape = RoundedCornerShape(12.dp)
    )){
        Surface(
            elevation = 10.dp, modifier = modifier
                .align(Alignment.Center)
                .fillMaxWidth(0.7f)
                .fillMaxHeight(0.2f),
            shape = RoundedCornerShape(12.dp)
        ) {
            Box(
                modifier = Modifier
                    .background(
                        color = AlmostWhiteBackground,
                        shape = RoundedCornerShape(12.dp)
                    )
            ) {
                //Header
                val bgdColor = Color(ColorUtils.blendARGB(
                    getTopHeaderColor(noteColor).toArgb(),
                    0x000000,
                    0.4f
                ))
                Box(
                    modifier = modifier
                        .fillMaxWidth()
                        .background(bgdColor)
                        .align(Alignment.TopStart)
                        .padding(5.dp)
                ) {
                    Text(
                        text = context.getString(R.string.confirm_deletion),
                        modifier = modifier
                            .align(Alignment.CenterStart)
                            .padding(start = 10.dp),
                        color = AlmostWhiteBackground
                    )
                }
                Text(
                    text = context.getString(R.string.delete_this_note),
                    modifier = modifier.align(Alignment.Center),
                    color = AlmostBlackButtonColor
                )
                Row(
                    modifier
                        .fillMaxWidth(0.6f)
                        .align(Alignment.BottomEnd)
                        .padding(bottom = 5.dp).background(Color.Transparent),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = context.getString(R.string.yes),
                        modifier = modifier.clickable (
                            interactionSource = interactionSource,
                            indication = null
                        ){
                            onDeleteNote()
                        }.padding(5.dp), color = AlmostBlackButtonColor)
                    Text(
                        text = context.getString(R.string.no),
                        modifier = modifier.clickable (
                            interactionSource = interactionSource,
                            indication = null
                        ){
                            onConfirmDeleteNote(!shouldConfirmDeleteNote)
                        }.padding(5.dp), color = AlmostBlackButtonColor)
                }
            }
        }
    }
}

private fun getTopHeaderColor(noteColor: Long): Color {
    return when (noteColor) {
        ColorGreyLong -> Color(ColorGreyDarkHue)
        ColorBlueLong -> Color(ColorBlueDarkHue)
        ColorLightBlueLong -> Color(ColorLightBlueDarkHue)
        ColorGreenLong -> Color(ColorGreenDarkHue)
        ColorBrownLong -> Color(ColorBrownDarkHue)
        ColorGreyMagentaLong -> Color(ColorPurpleDarkHue)
        ColorRedLong -> Color(ColorRedDarkHue)
        ColorYellowLong -> Color(ColorYellowDarkHue)
        else -> Color(ColorGreyDarkHue)
    }
}