package com.app.chukanotes.presentation.todo_list.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.chukanotes.ui.theme.AlmostWhiteBackground

@Composable
fun BasicTitleTextField(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    noteTitle: String,
    onTitleFieldValueChanged: (String) -> Unit,
    cursorBrush: Brush = SolidColor(Color.Green),
) {
    BasicTextField(
        value = noteTitle,
        onValueChange = { newValue -> onTitleFieldValueChanged(newValue) },
        modifier = modifier.padding(top = 3.dp),
        enabled = enabled,
        textStyle = TextStyle(
            fontSize = 22.sp,
            fontWeight = FontWeight.Bold,
            color = AlmostWhiteBackground,
            textDecoration = TextDecoration.None,
        ),
        cursorBrush = cursorBrush,
        keyboardOptions = KeyboardOptions.Default.copy(
            capitalization = KeyboardCapitalization.Sentences, autoCorrect = true
        )
    )
}