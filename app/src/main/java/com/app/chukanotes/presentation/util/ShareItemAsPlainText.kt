package com.app.chukanotes.presentation.util

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.app.chukanotes.R

@Composable
internal fun ShareItemAsPlainText(
  context: Context,
  modifier: Modifier,
  interactionSource: MutableInteractionSource,
  stringToShare: StringBuilder,
  shareTitle: String
) {
  Image(
    painter = painterResource(id = R.drawable.share_icon),
    contentDescription = context.getString(R.string.share_note),
    modifier = modifier
      .padding(horizontal = 10.dp, vertical = 8.dp)
      .size(40.dp)
      .clickable(
        interactionSource = interactionSource,
        indication = null
      ) {
        val sendIntent: Intent = Intent().apply {
          action = Intent.ACTION_SEND
          putExtra(Intent.EXTRA_TEXT, stringToShare.toString())
          type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, shareTitle)
        context.startActivity(shareIntent)
      }
  )
}