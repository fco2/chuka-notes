package com.app.chukanotes.presentation.todo_list.components

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.app.chukanotes.R
import com.app.chukanotes.presentation.modal_components.ModalType
import com.app.chukanotes.ui.theme.DarkerGray
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
fun topAppBarCustom(
    modifier: Modifier,
    context: Context,
    modalType: ModalType,
    modalTitle: String,
    coroutineScope: CoroutineScope,
    modalBottomSheetState: ModalBottomSheetState,
    onLabelModalOpened: () -> Unit
): Pair<String, ModalType> {
    var modalType1 = modalType
    var modalTitle1 = modalTitle
    Row(
        modifier = modifier
            .fillMaxWidth()
            .height(64.dp)
            .padding(0.dp)
            .background(DarkerGray)
            .padding(10.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.End
    )
    {
        Image(
            painter = painterResource(id = R.mipmap.img_choose_label),
            contentDescription = context.getString(R.string.choose_label),
            modifier = modifier
                .padding(horizontal = 12.dp, vertical = 8.dp)
                .clickable { onLabelModalOpened() }
        )
        Image(
            painter = painterResource(id = R.mipmap.img_choose_color),
            contentDescription = context.getString(R.string.choose_color),
            modifier = modifier
                .padding(horizontal = 12.dp, vertical = 8.dp)
                .clickable {
                    //set modal type and title
                    modalType1 = ModalType.Color
                    modalTitle1 = context.getString(R.string.select_note_background_color)
                    coroutineScope.launch { modalBottomSheetState.show() }
                }
        )
    }
    return Pair(modalTitle1, modalType1)
}