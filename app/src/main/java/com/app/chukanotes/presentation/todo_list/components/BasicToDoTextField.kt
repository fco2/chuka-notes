package com.app.chukanotes.presentation.todo_list.components

import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.sp
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.ui.theme.AlmostWhiteBackground
import com.app.chukanotes.ui.theme.InactiveTextColor


// https://tedblob.com/basic-text-field-jetpack-compose/

@Composable
fun BasicToDoTextField(
    toDoItem: ToDoItem,
    text: String,
    isCompleted: Boolean,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    cursorBrush: Brush = SolidColor(Color.Green),
    onTextFieldChanged: (ToDoItem, String) -> Unit
) {
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(Unit){
        if(!isCompleted && text.isEmpty())
            focusRequester.requestFocus()
    }

    BasicTextField(
        value = text,
        onValueChange = { newValue -> onTextFieldChanged(toDoItem, newValue) },
        modifier = modifier.focusRequester(focusRequester),
        enabled = enabled,
        textStyle = TextStyle(
            textDecoration = if (isCompleted) TextDecoration.LineThrough else TextDecoration.None,
            fontSize = 15.sp, color = if(isCompleted) InactiveTextColor else AlmostWhiteBackground,
        ),
        cursorBrush = cursorBrush,
        keyboardOptions = KeyboardOptions.Default.copy(
            capitalization = KeyboardCapitalization.Sentences,autoCorrect = true
        )
    )
}