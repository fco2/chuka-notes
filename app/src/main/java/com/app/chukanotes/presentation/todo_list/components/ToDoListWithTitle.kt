package com.app.chukanotes.presentation.todo_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.ui.theme.*

@Composable
fun ToDoListWithTitle(
    modifier: Modifier = Modifier,
    list: List<ToDoItem>,
    noteColor: Long,
    titleText: String,
    isTitleEmpty: Boolean,
    titleHint: String,
    addToDoIconId: Int,
    addIconContentDescription: String,
    addToDoHint: String,
    onCheckedChanged: (ToDoItem, Boolean) -> Unit,
    onDeleteToDo: (ToDoItem) -> Unit,
    onTextFieldChanged: (ToDoItem, String) -> Unit,
    onTitleFieldValueChanged: (String) -> Unit,
    onAddToDoItem: () -> Unit
) {
    val interactionSource = remember { MutableInteractionSource() }
    //if (list.isNotEmpty()) {
        LazyColumn(modifier = modifier.fillMaxWidth())
        {
            //title box
            item {
                Box(
                    modifier = modifier
                        .fillMaxWidth()
                        .padding(5.dp)
                ) {
                    //title text field
                    BasicTitleTextField(
                        modifier = modifier.fillMaxWidth(),
                        noteTitle = titleText,
                        onTitleFieldValueChanged = { newValue -> onTitleFieldValueChanged(newValue) }
                    )
                    //hint
                    if (isTitleEmpty) {
                        Text(
                            text = titleHint,
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Bold,
                            color = InactiveTextColor,
                            modifier = modifier.fillMaxWidth()
                        )
                    }
                }
            }
            items(list.filterNot{ it.isCompleted }, key = {item: ToDoItem ->  item.id as Any}) { it ->
                ToDoItemCompose(
                    toDoItem = it,
                    text = it.message,
                    isCompleted = it.isCompleted,
                    onCheckedChanged = { toDo, checked -> onCheckedChanged(toDo, checked) },
                    onDeleteToDo = { toDo -> onDeleteToDo(toDo) },
                    onTextFieldChanged = { toDo, newValue -> onTextFieldChanged(toDo, newValue) }
                )
            }
            //Add to-do text button
            items(1){
                Row(modifier = modifier
                    .fillMaxWidth()
                    .clickable (
                        interactionSource = interactionSource,
                        indication = null
                    ){ onAddToDoItem() }
                    .padding(start = 6.dp, top = 12.dp, bottom = 12.dp),
                    verticalAlignment = Alignment.CenterVertically) {
                    Icon(painter = painterResource(id = addToDoIconId),
                        contentDescription = addIconContentDescription,
                        tint = LightGreyDarkHue, modifier = modifier.padding(5.dp).size(26.dp))
                    Text(
                        text = addToDoHint,
                        color = LightGreyDarkHue,
                        fontSize = 15.sp,
                        modifier = modifier.weight(1f).padding(start = 10.dp)
                    )
                }
            }
            if (list.any { it.isCompleted }) {
                item {
                    Column(modifier = modifier.fillMaxWidth()) {
                        Divider(
                            modifier = Modifier
                                .padding(start = 2.dp, end = 2.dp, top = 8.dp)
                                .height(1.2.dp)
                                .background(
                                    //getGutterTopColor(noteColor)
                                    Color(
                                        ColorUtils.blendARGB(
                                            Color(noteColor).toArgb(),
                                            0x000000,
                                            0.3f
                                        )
                                    )
                                )
                        )
                        Divider(
                            modifier = Modifier
                                .padding(start = 2.dp, end = 2.dp, bottom = 8.dp)
                                .height(1.2.dp)
                                .background(
                                    //getGutterBottomColor(noteColor)
                                    Color(
                                        ColorUtils.blendARGB(
                                            Color(noteColor).toArgb(),
                                            0xffffff,
                                            0.7f
                                        )
                                    )
                                )
                        )
                    }
                }
            }
            items(list.filter{ it.isCompleted }, key = {item: ToDoItem ->  item.id as Any}) { it ->
                ToDoItemCompose(
                    toDoItem = it,
                    text = it.message,
                    isCompleted = it.isCompleted,
                    onCheckedChanged = { toDo, checked -> onCheckedChanged(toDo, checked) },
                    onDeleteToDo = { toDo -> onDeleteToDo(toDo) },
                    onTextFieldChanged = { toDo, newValue -> onTextFieldChanged(toDo, newValue) }
                )
            }
            //to push list up a bit above keyboard
            item {
                Spacer(modifier = Modifier.height(400.dp))
            }
        }
    //}
}

private fun getGutterTopColor(noteColor: Long): Color {
    return when (noteColor) {
        ColorGreyLong -> Color(ColorGreyDarkHue)
        ColorBlueLong -> Color(ColorBlueDarkHue)
        ColorLightBlueLong -> Color(ColorLightBlueDarkHue)
        ColorGreenLong -> Color(ColorGreenDarkHue)
        ColorBrownLong -> Color(ColorBrownDarkHue)
        ColorGreyMagentaLong -> Color(ColorPurpleDarkHue)
        ColorRedLong -> Color(ColorRedDarkHue)
        ColorYellowLong -> Color(ColorYellowDarkHue)
        else -> Color(ColorGreyDarkHue)
    }
}

private fun getGutterBottomColor(noteColor: Long): Color {
    return when (noteColor) {
        ColorGreyLong -> Color(ColorGreyLightHue)
        ColorBlueLong -> Color(ColorBlueLightHue)
        ColorLightBlueLong -> Color(ColorLightBlueLightHue)
        ColorGreenLong -> Color(ColorGreenLightHue)
        ColorBrownLong -> Color(ColorBrownLightHue)
        ColorGreyMagentaLong -> Color(ColorPurpleLightHue)
        ColorRedLong -> Color(ColorRedLightHue)
        ColorYellowLong -> Color(ColorYellowLightHue)
        else -> Color(ColorGreyLightHue)
    }
}