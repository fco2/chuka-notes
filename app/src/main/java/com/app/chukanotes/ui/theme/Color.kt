package com.app.chukanotes.ui.theme

import androidx.compose.ui.graphics.Color

//Constants
const val ColorGreyLong = 0xff263238 //Dark grayish blue
const val ColorLightPurpleLong = 0xff465375
const val ColorGrayishYellowLong = 0xff434731
const val ColorGrayishGreenLong = 0xff47583c
const val ColorBlueLong = 0xff154360   //dark blue
const val ColorLightBlueLong = 0xff2f4347 //dark grayish cyan
const val ColorGreenLong = 0xff0e4742  //dark cyan
//back up green 173e32
const val ColorBrownLong = 0xff5d473e  //dark grayish orange
const val ColorGreyMagentaLong = 0xff6d5269  //dark grayish magenta
const val ColorRedLong = 0xff794040  //Dark moderate red
const val ColorYellowLong = 0xff6b5e04  //Very dark yellow [Olive tone].  --> Olive yellow


val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val BabyBlue = Color(0xFF0a82f9)
val DarkBabyBlue = Color(0xFF055cb2)
val AlmostWhiteBackground = Color(0xFFececec)
val LightGrey = Color(0xFFc6c6c6)
val LightGreyDarkHue = Color(0xFF898989)
val DarkerGray = Color(0xff222d32)
val NotSoBlack = Color(0xFF3a4d55)
val NotSoBlackLightHue = Color(0xff617076)
val InactiveTextColor = Color(0xFF5c6975)
val InactiveTextColorLightHue = Color(0xFF97a3ad)
val FabButtonBackground = Color(0xffd14262)
val DeleteRedColor = Color(0xFF7c2d39)
val SmallerFabBackground = Color(0xFF9c485a)
val AlmostBlackButtonColor = Color(0xFF23272b)

val CheckboxColor = Color(0xff8CC777)


val ColorGrey = Color(ColorGreyLong)
val ColorBlue = Color(ColorBlueLong)
val ColorLightBlue = Color(ColorLightBlueLong)
val ColorGreen = Color(ColorGreenLong)
val ColorBrown= Color(ColorBrownLong) //#6b5148
val ColorGreyMagenta = Color(ColorGreyMagentaLong)
val ColorRed = Color(ColorRedLong)

//Light hue for colors
const val ColorBrownLightHue = 0xffa07e71
const val ColorLightBlueLightHue = 0xff5e868e
const val ColorBlueLightHue = 0xff277bb0
const val ColorGreenLightHue = 0xff1b897f
const val ColorPurpleLightHue = 0xffa98ca4
const val ColorRedLightHue = 0xffb77777
const val ColorGreyLightHue = 0xff4e6672
const val ColorYellowLightHue = 0xffb7a007

//dark hue for colors
const val ColorBrownDarkHue = 0xff2e231f
const val ColorLightBlueDarkHue = 0xff27383b
const val ColorBlueDarkHue = 0xff0e2d40
const val ColorGreenDarkHue = 0xff082624
const val ColorPurpleDarkHue = 0xff2a1f28
const val ColorRedDarkHue = 0xff2c1717
const val ColorGreyDarkHue = 0xff161d21
const val ColorYellowDarkHue = 0xff1f1c01
