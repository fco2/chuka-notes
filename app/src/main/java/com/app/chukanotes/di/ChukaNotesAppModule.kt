package com.app.chukanotes.di

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.app.chukanotes.data.local.ToDoItemsDatabase
import com.app.chukanotes.data.local.ToDoItemsDatabase.Companion.DATABASE_NAME
import com.app.chukanotes.data.local.util.CustomDateTimeAdapter
import com.app.chukanotes.data.local.util.HashSymbolAdapter
import com.app.chukanotes.data.local.util.PlusCharacterAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ChukaNotesAppModule {

    @Provides
    @Singleton
    fun getDatabase(@ApplicationContext context: Context) = Room.databaseBuilder(
            context, ToDoItemsDatabase::class.java, DATABASE_NAME
    ).addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6,
        MIGRATION_6_7, MIGRATION_7_8, MIGRATION_8_9)
            .build()

    @Singleton
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(HashSymbolAdapter())
        .add(CustomDateTimeAdapter())
        .add(PlusCharacterAdapter())
        .add(KotlinJsonAdapterFactory()).build()

    //Room Migrations
    private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            //Create temp table for note
            database.execSQL("CREATE TABLE IF NOT EXISTS `note_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "`title` TEXT NOT NULL DEFAULT '', `label` TEXT NOT NULL DEFAULT '', `color` TEXT NOT NULL DEFAULT '', `last_updated` TEXT) ")
            // Copy the data
            database.execSQL("INSERT INTO note_new (id, title, label, color, last_updated) " +
                "SELECT id, title, label, color, last_updated FROM note")
            // Remove the old table
            database.execSQL("DROP TABLE note")
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE note_new RENAME TO note")

            //FOR to_do_item table
            //Create temp table for to_do_item
            database.execSQL("CREATE TABLE IF NOT EXISTS `to_do_item_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "`note_id` INTEGER NOT NULL DEFAULT 0, `message` TEXT NOT NULL DEFAULT '', `is_completed` INTEGER NOT NULL DEFAULT 0," +
                " `date_completed` TEXT)")
            // Copy the data
            database.execSQL("INSERT INTO to_do_item_new (id, note_id, message, is_completed, date_completed) " +
                    "SELECT id, note_id, message, is_completed, date_completed FROM to_do_item")
            // Remove the old table
            database.execSQL("DROP TABLE to_do_item")
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE to_do_item_new RENAME TO to_do_item")
        }
    }
    //Add column rank to use for reordering of notes by user.
    private val MIGRATION_2_3: Migration = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE note ADD COLUMN rank INTEGER")
        }
    }
    //Add column note_format to distinguish between plain text and todo_list
    private val MIGRATION_3_4: Migration = object : Migration(3, 4){
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE note ADD COLUMN note_format TEXT")
        }
    }
    //Create table Label
    private val MIGRATION_4_5: Migration = object: Migration(4, 5){
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `label` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "`label_name` TEXT, `is_active` INTEGER NOT NULL DEFAULT 0)")
        }
    }

    private val MIGRATION_5_6: Migration = object: Migration(5, 6){
        override fun migrate(database: SupportSQLiteDatabase) {
            //FOR to_do_item table - start with this because it is foreign keyed to note table.
            //Create temp table for to_do_item
            database.execSQL("CREATE TABLE IF NOT EXISTS `to_do_item_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "`note_id` INTEGER NOT NULL, `message` TEXT NOT NULL, `is_completed` INTEGER NOT NULL, `date_completed` TEXT," +
                    " FOREIGN KEY (note_id) REFERENCES note(id) ON DELETE CASCADE ON UPDATE CASCADE)")
            //FOR note table
            //Create temp table for note with updated color as Long
            database.execSQL("CREATE TABLE IF NOT EXISTS `note_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "`title` TEXT NOT NULL, `label` TEXT NOT NULL, `color` INTEGER NOT NULL, `last_updated` TEXT, `rank` INTEGER NOT NULL, `note_format` TEXT)")
            // Copy the data
            database.execSQL("INSERT INTO note_new (id, title, label, last_updated, rank, note_format, color) " +
                    "SELECT id, title, label, last_updated, rank, note_format, "+ 0xff263238 + " FROM note")
            // Copy the data
            database.execSQL("INSERT INTO to_do_item_new (id, note_id, message, is_completed, date_completed) " +
                    "SELECT id, note_id, message, is_completed, date_completed FROM to_do_item")
            // Remove the old table
            database.execSQL("DROP TABLE note")
            // Remove the old table
            database.execSQL("DROP TABLE to_do_item")
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE note_new RENAME TO note")
            // Change the table name to the correct one
            database.execSQL("ALTER TABLE to_do_item_new RENAME TO to_do_item")
            //add this last
            database.execSQL("CREATE INDEX index_to_do_item_note_id on to_do_item(note_id)")
        }
    }

    private val MIGRATION_6_7: Migration = object: Migration(6, 7){
        override fun migrate(database: SupportSQLiteDatabase) {
            with(database) {
                //Blasts and recreates the label table
                execSQL("CREATE TABLE IF NOT EXISTS label_backup (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                        "`note_id` INTEGER NOT NULL DEFAULT 0, `title` TEXT)")
                execSQL("DROP TABLE label")
                execSQL("ALTER TABLE label_backup RENAME to label")
                //this removes the label column from note table
                //FOR to_do_item table - start with this because it is foreign keyed to note table.
                //Create temp table for to_do_item
                execSQL("CREATE TABLE IF NOT EXISTS `to_do_item_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`note_id` INTEGER NOT NULL DEFAULT 0, `message` TEXT NOT NULL DEFAULT '', `is_completed` INTEGER NOT NULL DEFAULT 0," +
                    " `date_completed` TEXT, FOREIGN KEY (note_id) REFERENCES note(id) ON DELETE CASCADE ON UPDATE CASCADE)")
                //FOR note table
                //Create temp table for note with updated color as Long
                execSQL("CREATE TABLE IF NOT EXISTS `note_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`title` TEXT NOT NULL DEFAULT '', `color` INTEGER NOT NULL DEFAULT 0, `last_updated` TEXT, `rank` INTEGER NOT NULL DEFAULT 0," +
                    " `note_format` TEXT)")
                // Copy the data
                execSQL("INSERT INTO note_new (id, title, last_updated, rank, note_format, color) " +
                        "SELECT id, title, last_updated, rank, note_format, color FROM note")
                // Copy the data
                execSQL("INSERT INTO to_do_item_new (id, note_id, message, is_completed, date_completed) " +
                        "SELECT id, note_id, message, is_completed, date_completed FROM to_do_item")
                // Remove the old table
                execSQL("DROP TABLE note")
                // Remove the old table
                execSQL("DROP TABLE to_do_item")
                // Change the table name to the correct one
                execSQL("ALTER TABLE note_new RENAME TO note")
                // Change the table name to the correct one
                execSQL("ALTER TABLE to_do_item_new RENAME TO to_do_item")
                //add this last
                execSQL("CREATE INDEX index_to_do_item_note_id on to_do_item(note_id)")
                //Create index for the label.note_id
                execSQL("CREATE UNIQUE INDEX index_label_note_id on label(note_id)")
            }
        }
    }

    private val MIGRATION_7_8: Migration = object: Migration(7, 8) {
        override fun migrate(database: SupportSQLiteDatabase) {
            with(database){
                //Blasts and recreates the label table
                execSQL("CREATE TABLE IF NOT EXISTS label_backup (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`note_id` INTEGER NOT NULL DEFAULT 0, `title` TEXT)")
                execSQL("DROP TABLE label")
                execSQL("ALTER TABLE label_backup RENAME to label")
                //Create index for the label.note_id
                execSQL("CREATE UNIQUE INDEX index_label_note_id on label(note_id)")
                execSQL("INSERT INTO label (id, note_id, title) SELECT id, note_id, title FROM label")
            }
        }
    }

    //This migration adds is_pinned column to note table
    private val MIGRATION_8_9: Migration = object: Migration(8, 9) {
        override fun migrate(database: SupportSQLiteDatabase) {
            with(database){
                //FOR to_do_item table - start with this because it is foreign keyed to note table.
                //Create temp table for to_do_item
                execSQL("CREATE TABLE IF NOT EXISTS `to_do_item_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`note_id` INTEGER NOT NULL DEFAULT 0, `message` TEXT NOT NULL DEFAULT '', `is_completed` INTEGER NOT NULL DEFAULT 0," +
                    " `date_completed` TEXT, FOREIGN KEY (note_id) REFERENCES note(id) ON DELETE CASCADE ON UPDATE CASCADE)")
                //FOR note table
                //Create temp table for note with updated color as Long
                execSQL("CREATE TABLE IF NOT EXISTS `note_new` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`is_pinned` INTEGER NOT NULL, `title` TEXT NOT NULL, `color` INTEGER NOT NULL, " +
                        "`last_updated` TEXT, `rank` INTEGER NOT NULL, `note_format` TEXT)")
                // Copy the data
                execSQL("INSERT INTO note_new (id, title, last_updated, rank, note_format, color) " +
                        "SELECT id, title, last_updated, rank, note_format, color FROM note")
                // Copy the data
                execSQL("INSERT INTO to_do_item_new (id, note_id, message, is_completed, date_completed) " +
                        "SELECT id, note_id, message, is_completed, date_completed FROM to_do_item")
                // Remove the old table
                execSQL("DROP TABLE note")
                // Remove the old table
                execSQL("DROP TABLE to_do_item")
                // Change the table name to the correct one
                execSQL("ALTER TABLE note_new RENAME TO note")
                // Change the table name to the correct one
                execSQL("ALTER TABLE to_do_item_new RENAME TO to_do_item")
                //add this last
                execSQL("CREATE INDEX index_to_do_item_note_id on to_do_item(note_id)")
            }
        }

    }

    private fun convertColorHexToInt(){
        //works like a charm
        val myStr = "#eeeeee"
        val intColor: Int = Integer.parseInt(myStr.replaceFirst("#", ""), 16)
        println(intColor)

        val hexColor = String.format("#%06X", 0xFFFFFF and intColor)
        println(hexColor)
    }
}