package com.app.chukanotes.domain.models

import com.app.chukanotes.data.local.entities.NoteEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.time.OffsetDateTime

@JsonClass(generateAdapter = true)
data class Note(
    @Json(name = "title")
    var title: String = "",
    @Json(name = "color")
    var color: Long = 0xff263238,
    var rank: Long = 0L,
    var noteFormat: String? = "",
    var id: Long? = -1L,
    @Json(name = "lastUpdated")
    var lastUpdated: OffsetDateTime? = OffsetDateTime.now(),
    var isPinned: Boolean = false,
){
    fun toNoteEntity(): NoteEntity {
        return NoteEntity(
            title = title,
            color = color,
            lastUpdated = lastUpdated,
            rank = rank,
            noteFormat = noteFormat,
            isPinned = isPinned,
            id = id
        )
    }
}
