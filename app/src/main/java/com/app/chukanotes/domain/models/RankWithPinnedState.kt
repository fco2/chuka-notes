package com.app.chukanotes.domain.models

data class RankWithPinnedState(
    val rank: Long,
    val isPinned: Boolean
)
