package com.app.chukanotes.domain.repositories

import com.app.chukanotes.core.Resource
import com.app.chukanotes.data.local.ToDoItemsDatabase
import com.app.chukanotes.data.local.entities.NoteEntity
import com.app.chukanotes.data.local.entities.ToDoItem
import com.app.chukanotes.domain.models.Label
import com.app.chukanotes.domain.models.Note
import com.app.chukanotes.domain.models.NoteWithPreview
import com.app.chukanotes.domain.models.RankWithPinnedState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NoteAndToDoRepository @Inject constructor(private val database: ToDoItemsDatabase){
    //GET queries
    suspend fun getAllRankWithPinnedState(): Flow<Resource<List<RankWithPinnedState>>> = flow {
        emit(Resource.Loading())
        val allRankWithPinnedState = database.toDoItemsDao.getAllRankWithPinnedState()
        emit(Resource.Success(allRankWithPinnedState))
    }

    suspend fun getFilteredNotesAll(searchQuery: String): Flow<List<NoteWithPreview>> = flow {
        val searchQueryLowercase = searchQuery.lowercase().trim()
        val notesWithPreview: List<NoteWithPreview> = database.toDoItemsDao.getAllNotes()
            .filter {
                it.label?.let{labelIt -> labelIt.title!!.lowercase().contains(searchQueryLowercase) } ?: false
                        || it.toDoItems.any { toDo ->
                    toDo.message.lowercase().contains(searchQueryLowercase)
                } ||
                        it.note.title.lowercase().contains(searchQueryLowercase)}.map { it.toNoteWithPreview() }.distinct()
        emit(notesWithPreview)
    }

    suspend fun getSingleNote(noteId: Long): NoteEntity? = database.toDoItemsDao.getSingleNote(noteId)

    suspend fun getToDoItemForPlainNote(noteId: Long): ToDoItem? =
        database.toDoItemsDao.getToDoItemForPlainNote(noteId)

    suspend fun getAllItemsFromToDoWith(noteId: Long): List<ToDoItem> = database.toDoItemsDao.getAllItemsFromToDoWith(noteId)

    suspend fun getHighestRankForUnPinnedState(): Long? = database.toDoItemsDao.getHighestRankForUnPinnedState()

    //For Labels
    fun getAllLabels(): Flow<Resource<List<Label>>> = flow {
        emit(Resource.Loading())
        val labels = database.toDoItemsDao.getAllLabels()
        emit(Resource.Success(labels.map { it.toLabel() }))
    }

    //UPSERT queries
    suspend fun upsertAllNotesWhereNotesIn(notes: List<NoteEntity>){
        database.toDoItemsDao.upsertAllNotesWhereNotesIn(notes)
    }

    suspend fun upsertToDoItem(toDoItem: ToDoItem): Long = database.toDoItemsDao.upsertToDoItem(toDoItem)

    suspend fun upsertToDoItemsFor(toDoItems: List<ToDoItem>){
        database.toDoItemsDao.upsertToDoItemsFor(toDoItems)
    }

    suspend fun upsertNoteWithTodos(note: Note, toDoItems: List<ToDoItem>){
        database.toDoItemsDao.upsertNoteWithTodos(note.toNoteEntity(), toDoItems)
    }

    suspend fun upsertNoteWithSingleTodo(note: Note, toDoItem: ToDoItem): Long{
        return  database.toDoItemsDao.upsertNoteWithSingleTodo(note.toNoteEntity(), toDoItem)
    }

    suspend fun addLabel(label: Label): Long{
        return database.toDoItemsDao.addLabel(label.toLabelEntity())
    }

    //DELETE queries
    suspend fun deleteNote(note: NoteEntity) = database.toDoItemsDao.deleteNote(note)

    suspend fun deleteNotesWithNoteObject(notes: List<NoteEntity>) = database.toDoItemsDao.deleteNotesWithNoteObject(notes)

    suspend fun deleteToDoItemFromNote(noteId: Long, toDoId: Long) = database.toDoItemsDao.deleteToDoItemFromNote(noteId, toDoId)

    suspend fun deleteLabel(labelTitle: String){
        database.toDoItemsDao.deleteLabel(labelTitle)
    }
}