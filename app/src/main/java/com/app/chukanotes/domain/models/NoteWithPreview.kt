package com.app.chukanotes.domain.models


data class NoteWithPreview(
    val note: Note,
    val labelName: String = "",
    var isSelected: Boolean = false,
    var previewOne: String = "",
    var previewTwo: String = "",
    var previewThree: String = "",
){
    val id: Long = note.id!!
}