package com.app.chukanotes.domain.models

import com.app.chukanotes.data.local.entities.LabelEntity

data class Label(
    var title: String = "",
    var noteId: Long = -1L,
    var id: Long? = null
){
    fun toLabelEntity(): LabelEntity {
        return LabelEntity(
            id = id,
            title = title,
            noteId = noteId
        )
    }
}
