package com.app.chukanotes.core

object Constants {

    //Note formats
    const val TO_DO_LIST_NOTE_FORMAT = "ToDoList"
    const val PLAIN_TEXT_NOTE_FORMAT = "PlainText"

    //uri
    const val BASE_URI = "https://chukanotes.com"

}